"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Discord = require("discord.js");
const logger_1 = require("./helpers/logger");
const bot = new Discord.Client();
exports.bot = bot;
bot.on('ready', function (event) {
    logger_1.default.info("Bot connected!");
});
bot.on('disconnect', function () {
    logger_1.default.error('Disconnected... Attempting to log in every 10 seconds');
});
//# sourceMappingURL=Bot.js.map