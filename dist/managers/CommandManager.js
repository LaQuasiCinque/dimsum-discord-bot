"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const discord_js_1 = require("discord.js");
const Manager_1 = require("./Manager");
const utility_1 = require("../helpers/utility");
const util_1 = require("util");
require("./managers");
class CommandManager extends Manager_1.Manager {
    constructor() {
        super(...arguments);
        this._name = 'commandManager';
        this._hooks = {
            message: []
        };
        this.commands = {};
    }
    install() {
        let HOOK = this.store.state.hook;
        this.bot.on('message', (message) => this.onMessageHook(message));
        this.hook = HOOK instanceof RegExp ? HOOK : new RegExp(`^${HOOK} `, 'i');
        let self = this;
        this.installManyCommands({
            hello: {
                handler: this.commandHello.bind(this),
                description: "Say hello to the bot",
            },
            commands: {
                handler: this.commandDisplayCommands.bind(this),
                description: "Get a list of commands"
            }
        });
    }
    parse(message) {
        let raw = message.content;
        let dataManager = this.managers.dataManager;
        // Reset hook index
        this.hook.lastIndex = -1;
        if (this.hook.test(raw)) {
            const args = this._getArguments(raw);
            const command = this._getCommand(args);
            return {
                command,
                args,
                raw
            };
        }
        return;
    }
    installCommand(commandName, command) {
        if (!this.commands[commandName]) {
            this.commands[commandName] = command;
        }
        else {
            this.logger.info(`Failed to install command ${commandName}.`);
        }
    }
    installManyCommands(commandList) {
        for (let command in commandList) {
            if (commandList.hasOwnProperty(command)) {
                this.installCommand(command, commandList[command]);
            }
        }
    }
    onMessageHook(message) {
        this._handleCommand(message);
    }
    _getCommand(args) {
        let command = args.splice(0, 1)[0];
        return command ? command.toLowerCase() : "";
    }
    _getArguments(raw) {
        return raw
            .replace(this.hook, '')
            .trim()
            .split(' ');
    }
    _handleCommand(message) {
        let commandData;
        commandData = this.parse(message);
        if (commandData) {
            let { command } = commandData;
            if (this.commands[command]) {
                try {
                    this.commands[command].handler(message, commandData);
                }
                catch (e) {
                    this.logger.error(e);
                    try {
                        if (utility_1.isString(e)) {
                            message.react(e);
                        }
                        else {
                            if (util_1.isBoolean(e)) {
                                message.react(e ? '😒' : '😡');
                            }
                            else {
                                message.react('😡');
                            }
                        }
                    }
                    catch (l) {
                        message.react('😡')
                            .then(reaction => {
                            message.react('💩');
                        });
                    }
                }
            }
        }
    }
    getCommandNames() {
        return Object.keys(this.commands);
    }
    extend() {
    }
    commandHello(message) {
        message.channel.send(`What ho, ${message.author.username}!`, { tts: true });
    }
    commandDisplayCommands(message) {
        let commandNames = this.getCommandNames().sort();
        let embed = new discord_js_1.RichEmbed();
        embed.setTitle(`${this.store.state.originalName} Commands`)
            .setDescription(`This bot currently has ${commandNames.length} commands.

To use a command prepend one of the following characters/emojis: 
:fire:, :100:, :ok_hand:, :ok_hand::skin-tone-1:, :ok_hand::skin-tone-2:, :ok_hand::skin-tone-3:, :ok_hand::skin-tone-4:, :ok_hand::skin-tone-5:, $, or £ (God Bless the Queen).

The commands are:`);
        commandNames.forEach(item => {
            embed.addField(item, this.commands[item].description + '.');
        });
        message.channel.send(embed);
        return true;
    }
}
exports.default = CommandManager;
//# sourceMappingURL=CommandManager.js.map