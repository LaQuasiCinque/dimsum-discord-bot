"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Manager_1 = require("./Manager");
const utility_1 = require("../helpers/utility");
const logger_1 = require("../helpers/logger");
const path = require("path");
const fs = require("fs");
require("./managers");
class DataManager extends Manager_1.Manager {
    constructor() {
        super(...arguments);
        this._name = 'dataManager';
    }
    install() {
        logger_1.default.info("Data Path:" + DataManager.BASE_PATH);
    }
    /**
     * Writes data to a datafile
     *
     * @param {string} filepath
     * @param {(string | null)} key
     * @param {*} value
     * @returns {boolean}
     */
    setData(filepath, key, value) {
        try {
            let data = this._openDataFile(filepath);
            if (!utility_1.isUnset(data)) {
                if (!this._setData(data, key, value)) {
                    return false;
                }
            }
            else {
                data = {};
                let keys = key ? key.split('.') : [];
                let lastKey = keys.pop();
                let lastObj = keys.reduce((prev, curr) => {
                    prev[curr] = {};
                    return prev[curr];
                }, {});
                // a key must be provided;
                if (lastKey) {
                    lastObj[lastKey] = value;
                }
                else {
                    console.log(arguments);
                    return false;
                }
            }
            return this._writeDataFile(filepath, data);
        }
        catch (e) {
            console.log(e);
            return false;
        }
    }
    /**
     * Gets data from a datafile
     *
     * @param {string} filepath
     * @param {(string | null)} key
     * @returns {*}
     */
    getData(filepath, key) {
        try {
            let data = this._openDataFile(filepath);
            return data ? (key ? utility_1.getNested(data, key) : data) : null;
        }
        catch (e) {
            return null;
        }
    }
    /**
     * Attempts to stop people from accessing files that shouldn't be accessed
     * Try saying this with a lisp :(
     * @param filepath
     */
    parsePath(filepath, extension) {
        filepath = filepath
            .replace(/^\~\./g, '')
            .replace(/[\/\\]{2,}/gi, '/')
            .replace(/\/*$/, '');
        if (extension && extension.trim()) {
            extension = extension.trim().replace(/^\.*/, '.');
            filepath = filepath + extension;
        }
        return filepath;
    }
    /**
     * Opens a data file, returns null if the file cannot be found
     *
     * @param {string} filepath
     * @returns {(object | null)}
     */
    _openDataFile(filepath) {
        let data = null;
        filepath = this.parsePath(filepath, '.json');
        filepath = path.resolve(DataManager.BASE_PATH, filepath);
        try {
            data = JSON.parse(fs.readFileSync(filepath, 'utf8'));
        }
        catch (e) {
            logger_1.default.info(`Failed to open ${filepath}.`);
        }
        return data;
    }
    /**
     * Writes data to a data file
     *
     * @param {string} filepath
     * @param {*} data
     * @returns {boolean}
     */
    _writeDataFile(filepath, data) {
        filepath = this.parsePath(filepath, '.json');
        filepath = path.resolve(DataManager.BASE_PATH, filepath);
        try {
            fs.writeFileSync(filepath, utility_1.isUnset(data) ? null : JSON.stringify(data, null, 3));
            return true;
        }
        catch (e) {
            return false;
        }
    }
    /**
     * Sets a piece of data. Returns success
     *
     * @param {(object | null)} data
     * @param {(string | null)} key
     * @param {*} value
     * @returns {boolean}
     */
    _setData(data, key, value) {
        // get the keys as an array of strings (or null)
        data = data || {};
        let keyArr = utility_1.isString(key) ? key.split('.') : [];
        let lastKey;
        let pointer = data;
        // if there is a key array, we require the last key to be our value
        if (keyArr) {
            lastKey = keyArr.pop();
        }
        for (let i = 0; i < keyArr.length; i++) {
            let key = keyArr[i];
            if (utility_1.isUnset(pointer[key])) {
                pointer[key] = {};
                pointer = pointer[key];
            }
            else {
                if (pointer[key] instanceof Object) {
                    pointer = pointer[key];
                }
                else {
                    return false;
                }
            }
        }
        // if the last key exists we can now set the value;
        if (lastKey) {
            pointer[lastKey] = value;
            return true;
        }
        else {
            return false;
        }
    }
    /**
     * Writes a stream to a file location
     */
    writeStream(filepath, filename, extension, stream) {
        return new Promise((resolve, reject) => {
            filepath = this.parsePath(filepath);
            filename = this.parsePath(filename, extension);
            let writeStream = fs.createWriteStream(path.resolve(DataManager.BASE_PATH, filepath, filename));
            writeStream.on('close', function () {
                resolve(writeStream);
            });
            stream.pipe(writeStream);
        });
    }
    /**
     * Returns if a data file already exists or not
     *
     * @param {string} filepath
     * @param {string} filename
     * @param {string} extension
     * @returns {boolean}
     * @memberof DataManager
     */
    dataExists(filepath, filename, extension) {
        filepath = this.parsePath(filepath);
        filename = this.parsePath(filename, extension);
        return fs.existsSync(path.resolve(DataManager.BASE_PATH, filepath, filename));
    }
    toDataPath(filepath, filename, extension) {
        filepath = this.parsePath(filepath);
        filename = this.parsePath(filename, extension);
        return path.resolve(DataManager.BASE_PATH, filepath, filename);
    }
}
DataManager.BASE_PATH = path.resolve(__dirname, '../../data');
exports.default = DataManager;
//# sourceMappingURL=DataManager.js.map