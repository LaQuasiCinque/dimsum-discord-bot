"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const ytdl = require("ytdl-core");
const Manager_1 = require("./Manager");
const Video = require("simple-youtube-api/src/structures/Video");
require("./managers");
class YoutubeManager extends Manager_1.Manager {
    constructor() {
        super(...arguments);
        this._name = 'youtubeManager';
        this._queue = [];
    }
    /**
     * install
     */
    install() {
        this.managers.commandManager.installCommand('dl', {
            handler: this.commandDownload.bind(this),
            description: 'Downloads a Youtube video, Adam only >:('
        });
    }
    downloadStream(link, options = { filter: 'audioonly' }) {
        return __awaiter(this, void 0, void 0, function* () {
            let id = this.getId(link);
            let name = `youtube-${id}`;
            let extension = options.filter == 'audioonly' ? 'mp3' : 'mp4';
            let fm = this.managers.fileManager;
            // file exists both as a file and database collection
            if (yield this.managers.fileManager.fileExists(name, extension)) {
                console.log("File exists 100%");
                return yield fm.getDataBaseEntry({ name, extension });
            }
            // file exists but no database entry
            if (yield this.managers.fileManager.fileActuallyExists(name, extension)) {
                console.log("file not found in database but exists");
                return yield fm.createDatabaseEntry({ name, extension, path: '' });
            }
            // Here the file doesn't exist in data
            let stream = this.getStream(link, options);
            if (stream) {
                let fileUploadData = yield this.managers.fileManager.uploadFile(name + '.' + extension, stream);
                if (!(yield this.managers.fileManager.fileInDatabase(name, extension))) {
                    console.log("file not in database and now exists");
                    return yield fm.createDatabaseEntry(fileUploadData);
                }
                console.log("file is in database but also exists");
                return yield fm.getDataBaseEntry({ name, extension });
            }
            else {
                throw new Error('failed to open stream');
            }
        });
    }
    getStream(link, options = {}) {
        let id = this.getId(link);
        if (id) {
            console.log(id);
            let link = 'https://www.youtube.com/watch?v=' + id;
            return ytdl(link, options);
        }
        else
            return null;
    }
    getId(url) {
        return Video.extractID(url);
    }
    queue(url) {
        // this._queue.push();
        return true;
    }
    commandDownload(message, { args }) {
        if (message.author.id !== '91922008987684864')
            return false;
        if (args.length) {
            let download = this.downloadStream(args[0], { filter: 'audioonly' });
            let reaction;
            download.catch(success => {
                if (reaction) {
                    reaction.remove();
                }
                if (success) {
                    message.react('✅');
                }
                else {
                    message.react('😡');
                }
            }).then(stuff => {
                message.react('🕐').then(_ => {
                    reaction = _;
                    download.then(l => {
                        reaction.remove().then(react => {
                            message.react('✅');
                        });
                    });
                });
            });
        }
    }
}
exports.default = YoutubeManager;
//# sourceMappingURL=YoutubeManager.js.map