"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const utility_1 = require("../helpers/utility");
/**
 * abstract class to provide the fundamentals for a manager to be able to be installed
 *
 * @export
 * @class Manager
 */
class Manager {
    /**
     * Creates an instance of Manager.
     * @param {Client} bot
     * @param {Store} store
     * @param {Winston} logger
     * @memberof Manager
     */
    constructor(bot, store, logger) {
        /**
         * manual way we're setting camelCase for basename
         *
         * @protected
         * @type {string}
         * @memberof Manager
         */
        this._name = 'manager';
        this.bot = bot;
        this.resolver = bot.resolver;
        this.store = store;
        this.logger = logger;
        utility_1.define(this)('managers', {
            value: store.state.managers
        });
        // this.resolver = new ClientDataResolver(bot);
    }
    /**
     * Gets the camelCase name for the managers class (stored in _name)
     * allows for possible overriding for more advanced behaviour
     *
     * @returns {string}
     * @memberof Manager
     */
    getName() {
        return this._name;
    }
    /**
     * installs the manager to the bot
     *
     * @memberof Manager
     */
    install() {
    }
}
exports.Manager = Manager;
//# sourceMappingURL=Manager.js.map