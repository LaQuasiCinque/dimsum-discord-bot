"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Manager_1 = require("./Manager");
const GuildSettings_1 = require("../database/models/GuildSettings");
class SettingsManager extends Manager_1.Manager {
    constructor() {
        super(...arguments);
        this._name = 'settingsManager';
    }
    /**
     * name
     */
    install() {
        this.managers.commandManager.installManyCommands({
            setafk: {
                handler: this.commandAFK.bind(this),
                description: 'Tells the bot the default AFK Channel for this guild'
            },
            setbot: {
                handler: this.commandBot.bind(this),
                description: 'Tells the bot a default text channel to use (to avoid spamming the main one)'
            }
        });
    }
    /**
     * setGuildSetting
     */
    setGuildSetting(guild, settingName, value) {
        return __awaiter(this, void 0, void 0, function* () {
            return GuildSettings_1.default.findOneOrCreate({ guildId: guild }, {
                guildId: guild
            }).then(guildSetting => {
                guildSetting.set(settingName, value);
                return guildSetting.save();
            });
        });
    }
    getGuildSetting(guild, settingName) {
        return __awaiter(this, void 0, void 0, function* () {
            let settingDoc = yield GuildSettings_1.default.findOne({ guildId: guild });
            let setting = settingDoc ? settingDoc.toObject() : {};
            return settingName ? setting[settingName] : setting;
        });
    }
    commandAFK(message, { args }) {
        let fullArgument = args.join(' ');
        let guild = message.guild.id;
        let channel = this.resolver.resolveChannel(fullArgument);
        if (channel && channel.type == 'voice') {
            let channelId = channel.id;
            this.setGuildSetting(guild, 'afkChannel', channelId)
                .then(success => {
                message.react('✅');
            }).catch(failed => {
                console.log(failed);
                message.react('😡');
            });
        }
        else {
            console.log("Could not find channel or channel was not voice channel");
            message.react('😡');
        }
    }
    commandBot(message, { args }) {
        let fullArgument = args.join(' ');
        let guild = message.guild.id;
        let channel = this.resolver.resolveChannel(fullArgument);
        if (channel && channel.type == 'text') {
            let channelId = channel.id;
            this.setGuildSetting(guild, 'botChannel', channelId)
                .then(success => {
                message.react('✅');
            }).catch(failed => {
                console.log(failed);
                message.react('😡');
            });
        }
        else {
            console.log("Channel was not text");
            message.react('😡');
        }
    }
}
exports.SettingsManager = SettingsManager;
//# sourceMappingURL=SettingsManager.js.map