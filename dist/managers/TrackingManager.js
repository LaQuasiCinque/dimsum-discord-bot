"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Manager_1 = require("./Manager");
require("./managers");
class TrackingManager extends Manager_1.Manager {
    constructor() {
        super(...arguments);
        this._name = 'trackingManager';
    }
    /**
     * installs manager's functions
     *
     * @memberof TrackingManager
     */
    install() {
        let self = this;
        this.bot.on('voiceStateUpdate', (night, marcus) => {
            this.nightMarcus(night, marcus);
        });
        this.bot.on('ready', () => {
            this.bot.guilds.forEach((guild) => {
                this._changeNickname(this.resolver.resolveGuildMember(guild, this.bot.user), this.store.state.originalName);
            });
        });
        this.store.addComputed('channels', {
            get() {
                let channels = self.store.state.channelIDs;
                let keys = Object.keys(channels);
                return keys.reduce((carry, item) => {
                    carry[item] = self.bot.channels.get(channels[item]);
                    return carry;
                }, {});
            }
        });
        this.managers.commandManager.installCommand('marcus', {
            handler: this.commandIsMarcus.bind(this),
            description: 'Makes the bot pretend to be Marcus and announce his present status.'
        });
    }
    /**
     * Night Marcus
     *
     * @param {GuildMember} night
     * @param {GuildMember} marcus
     * @memberof TrackingManager
     */
    nightMarcus(night, marcus) {
        let guild = night.guild || marcus.guild;
        let guildID = guild.id;
        if (night.voiceChannelID && !marcus.voiceChannelID) {
            if ((night.user.id == TrackingManager.MARCUS_ID) && new Date().getHours() > 20) {
                let botChannelId = this.managers.settingsManager.getGuildSetting(guildID, 'botChannel');
                if (botChannelId) {
                    let botChannel = guild.channels.find('id', botChannelId);
                    if (botChannel) {
                        botChannel.send('Night Marcus :wave: :crescent_moon: :sleeping: ');
                        botChannel.send('Night Marcus!', { tts: true }).then((message) => message.delete(100));
                    }
                }
            }
        }
    }
    /**
     * checks if marcus is in a voice channel
     *
     * @param {Message} message
     * @memberof TrackingManager
     */
    isMarcusHere(message) {
        let marcus = this._getMarcus(message.guild);
        let self = this._findSelf(message.guild);
        let msg = marcus.voiceChannel ? "I'm still here!" : "I've gone for lunch";
        this._changeNickname(self, 'Marcus').then(_ => {
            return message.channel.send(msg, { tts: true });
        }).then(msg => {
            return this._changeNickname(self, this.store.state.originalName);
        });
    }
    /**
     * attempts to change nickname (if bot has permission to)
     * returns a promise with itself as a GuildMember regardless of success
     *
     * @private
     * @param {GuildMember} self
     * @param {string} name
     * @returns {PromiseLike<GuildMember>}
     * @memberof TrackingManager
     */
    _changeNickname(self, name) {
        return this._canChangeNickname(self) ?
            self.setNickname(name) :
            new Promise((resolve, reject) => {
                return resolve(self);
            });
    }
    /**
     * returns Marcus as a GuildMember
     *
     * @private
     * @returns {GuildMember}
     * @memberof TrackingManager
     */
    _getMarcus(guild) {
        return guild.member(TrackingManager.MARCUS_ID);
    }
    /**
     * Meditates and finds self as a GuildMember
     *
     * @private
     * @returns {GuildMember}
     * @memberof TrackingManager
     */
    _findSelf(guild) {
        return guild.member(this.bot.user);
    }
    /**
     * returns if the bot has permission to change its name
     *
     * @private
     * @param {GuildMember} self
     * @returns {boolean}
     * @memberof TrackingManager
     */
    _canChangeNickname(self) {
        return self.hasPermission('MANAGE_NICKNAMES') && self.hasPermission("CHANGE_NICKNAME");
    }
    commandIsMarcus(message) {
        this.isMarcusHere(message);
    }
}
TrackingManager.MARCUS_ID = '162651416765267970';
exports.default = TrackingManager;
//# sourceMappingURL=TrackingManager.js.map