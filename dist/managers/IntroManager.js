"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Manager_1 = require("./Manager");
const utility_1 = require("../helpers/utility");
require("./managers");
const Intros_1 = require("../database/models/Intros");
const FileUpload_1 = require("../database/models/FileUpload");
class IntroManager extends Manager_1.Manager {
    constructor() {
        super(...arguments);
        this._name = 'introManager';
        this._num = 0;
        this._temp = false;
        this._introsLoaded = false;
    }
    install() {
        return __awaiter(this, void 0, void 0, function* () {
            this.downloadMissingIntros();
            this.bot.on('voiceStateUpdate', (oldMember, newMember) => {
                try {
                    if (this._introsLoaded) {
                        this.playIntro(oldMember, newMember);
                    }
                }
                catch (e) {
                    this.logger.error(e);
                }
                if (oldMember.voiceChannel && !newMember.voiceChannel) {
                    let voiceChannel = oldMember.voiceChannel;
                    let selfAsGM = this.resolver.resolveGuildMember(oldMember.guild, this.bot.user);
                    let numberOfOtherUsers = voiceChannel.members.filter(gm => {
                        return gm.id != selfAsGM.id;
                    }).size;
                    if (numberOfOtherUsers == 0 && selfAsGM.voiceChannel == oldMember.voiceChannel) {
                        this.managers.audioManager.leave(voiceChannel);
                    }
                }
            });
            this.managers.commandManager.installManyCommands({
                'intro': {
                    handler: this.commandPlayIntro.bind(this),
                    description: 'Plays the intro for a user'
                },
                'changeintro': {
                    handler: this.commandChangeIntro.bind(this),
                    description: 'Change your intro. arguments\n'
                        + '`<youtubeurl> [volume=0.2] [length=null] [begin=0]`'
                }
            });
        });
    }
    downloadMissingIntros() {
        return __awaiter(this, void 0, void 0, function* () {
            let data = yield Intros_1.default.aggregate([
                { $sort: { 'updatedDate': -1 } },
                {
                    $group: {
                        _id: '$userId',
                        id: { $first: '$_id' },
                        youtubeId: { $first: '$url' },
                        fileUploadId: { $first: "$fileUploadId" }
                    }
                }
            ]);
            let urls = data.map(item => {
                item.userId = item._id;
                item.introId = item.id;
                item.fullUrl = `https://www.youtube.com/watch?v=${item.youtubeId}`;
                delete item._id;
                delete item.id;
                return item;
            });
            let fm = this.managers.fileManager;
            let introData = yield Promise.all(urls.map((item, index) => __awaiter(this, void 0, void 0, function* () {
                let file;
                let fileUpload = yield this.managers.youtubeManager.downloadStream(item.fullUrl);
                item.fileUpload = fileUpload;
                return item;
            })));
            introData = introData.filter(item => {
                return item.fileUpload._id !== item.fileUploadId;
            });
            yield Promise.all(introData.map((item) => __awaiter(this, void 0, void 0, function* () {
                yield Intros_1.default.findByIdAndUpdate(item.introId, {
                    fileUploadId: item.fileUpload._id
                });
                console.log("intro attached");
                return true;
            })));
            this._introsLoaded = true;
        });
    }
    getUserIntroData(user) {
        return __awaiter(this, void 0, void 0, function* () {
            let userId = this.resolver.resolveUserID(user);
            let data = yield Intros_1.default.find({ 'userId': userId })
                .sort({ updatedDate: -1 })
                .limit(1)
                .exec();
            return data.length ? data[0].toObject() : null;
        });
    }
    playIntro(oldMember, newMember) {
        return __awaiter(this, void 0, void 0, function* () {
            let bot = this.bot;
            let self = this;
            let guild = newMember.guild;
            let selfGuildMember = this.resolver.resolveGuildMember(guild, bot.user);
            // if the user didn't mvoe to the afk channel
            if (newMember.voiceChannelID === (yield this.managers.settingsManager.getGuildSetting(guild.id, 'afkChannel')))
                return;
            // if the user didnt change channels
            if (oldMember && oldMember.voiceChannelID === newMember.voiceChannelID)
                return;
            // if there is no one else in the voice channel except the bot and the current user
            if (this.voiceChannelIsEmpty(newMember.voiceChannel, [
                selfGuildMember.id,
                newMember.id
            ])) {
                return;
            }
            let intro = yield this.getUserIntroData(newMember.user);
            let youtubeManager = this.managers.youtubeManager;
            let audioManager = this.managers.audioManager;
            if (intro != null) {
                if (newMember.voiceChannel) {
                    let lastPlayed = new Date(intro.lastPlayed || 0).getTime();
                    let now = new Date().getTime();
                    let currNum = ++this._num;
                    let connection = yield this._joinNewMemberChannel(newMember);
                    if (!intro.url) {
                        throw '😢';
                    }
                    let file = yield FileUpload_1.default.findById(intro.fileUploadId);
                    if (!file)
                        throw '😡';
                    let dispatcher = this.managers.audioManager.playFile(connection, this.managers.fileManager.getFilePath(file.name, file.extension, file.path), { seek: utility_1.coalesce(intro, 'start', 0), volume: intro.volume || 0.2 });
                    if (!dispatcher)
                        throw '😫';
                    dispatcher.on('end', function () {
                        self.logger.info("finished");
                    });
                    if (intro.length) {
                        dispatcher.on('start', function () {
                            let length = intro.length ? intro.length : 0;
                            setTimeout(function () {
                                if (dispatcher)
                                    dispatcher.end();
                            }, 1000 * length);
                        });
                    }
                }
                ;
            }
        });
    }
    _joinNewMemberChannel(newMember) {
        let bot = this.bot;
        let audioManager = this.managers.audioManager;
        let botVoiceChannel = audioManager.getVoiceChannel(newMember.guild);
        if (newMember.voiceChannel == botVoiceChannel) {
            return Promise.resolve(botVoiceChannel.connection);
        }
        else if (botVoiceChannel) {
            audioManager.leave(newMember.guild);
        }
        return newMember.voiceChannel.join();
    }
    _toTimeFormat(num = 0) {
        let minutes = ("00" + Math.floor(num / 60)).substr(-2);
        let seconds = ("00" + (num % 60)).substr(-2);
        return `${minutes}:${seconds}`;
    }
    commandPlayIntro(message, { args }) {
        if (!this._introsLoaded)
            return;
        if (!args.length)
            return;
        let user = message.guild.members.find((guildMember) => {
            return guildMember.user.username.toLowerCase() == args[0].toLowerCase();
        });
        if (!user)
            return;
        let messager = this.resolver.resolveGuildMember(message.guild, message.author);
        user.voiceChannelID = messager.voiceChannelID;
        this.playIntro(null, user);
    }
    commandChangeIntro(message, { args }) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!args.length)
                return;
            let userId = this.resolver.resolveUserID(message);
            let [url, volume = 0.2, length = null, start = 0,] = args;
            volume = Math.max(Math.min(volume, 0.2), 0);
            length = length == null ? 15 : Math.max(0, Math.min(length, 15));
            start = Math.max(0, start);
            let download;
            let reaction;
            try {
                reaction = yield message.react('🕐');
                download = yield this.managers.youtubeManager.downloadStream(url, { filter: 'audioonly' });
                let newIntro = yield Intros_1.default.create({
                    url,
                    volume,
                    start,
                    length,
                    fileUploadId: download._id,
                    userId
                });
                if (reaction) {
                    yield reaction.remove();
                    yield message.react('✅');
                }
            }
            catch (error) {
                if (reaction) {
                    yield reaction.remove();
                }
                console.log(error);
                yield message.react('😡');
            }
        });
    }
    voiceChannelIsEmpty(voiceChannel, exclude = []) {
        if (!voiceChannel)
            return;
        let members = voiceChannel.members.filter((member) => {
            return exclude.indexOf(member.id) > -1;
        });
        return members.size == 0;
    }
}
IntroManager.TIME_LIMIT = 0;
exports.default = IntroManager;
//# sourceMappingURL=IntroManager.js.map