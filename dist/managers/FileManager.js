"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Manager_1 = require("./Manager");
// import * as aws from 'aws-sdk';
const FileUpload_1 = require("../database/models/FileUpload");
// import * as request from 'request';
const fs = require("fs");
const path = require("path");
class FileManager extends Manager_1.Manager {
    constructor() {
        super(...arguments);
        this._name = 'fileManager';
    }
    install() {
        return __awaiter(this, void 0, void 0, function* () {
            this._root = path.resolve(__dirname, '../..');
        });
    }
    uploadFile(filename, stream, subdir = '') {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                subdir = subdir.replace(/^(?:\.\.|\/)+/g, '');
                let filepath = path.resolve(this.getRootDir(), 'data', subdir, filename);
                let upload = fs.createWriteStream(filepath);
                let self = this;
                stream.pipe(upload);
                upload.on("finish", function () {
                    let filenameParts = filename.split('.');
                    let extension = filenameParts.length > 1 ? filenameParts.pop() : '';
                    let name = filenameParts.join('.');
                    resolve({
                        path: path.resolve(self.getRootDir(), 'data', subdir),
                        name,
                        extension
                    });
                });
                upload.on("error", reject);
                stream.on("error", reject);
            });
        });
    }
    getFileStream(filename, stream, subdir) {
        return __awaiter(this, void 0, void 0, function* () {
            let fullPath = path.resolve(this.getRootDir(), subdir, filename);
            if (fs.existsSync(fullPath)) {
                return fs.createReadStream(fullPath);
            }
            else {
                throw new Error("File doesn't exist.");
            }
        });
    }
    getRootDir() {
        return this._root;
    }
    fileExists(name, extension, subdir = '') {
        return __awaiter(this, void 0, void 0, function* () {
            let fileActuallyExists = this.fileActuallyExists(name, extension, subdir);
            let fileInDatabase = yield this.fileInDatabase(name, extension, subdir);
            return fileInDatabase && fileActuallyExists;
        });
    }
    fileInDatabase(name, extension, subdir) {
        return __awaiter(this, void 0, void 0, function* () {
            let query = { name, extension };
            if (subdir) {
                query['path'] = subdir;
            }
            let fileUpload = yield FileUpload_1.default.findOne(query);
            let fileInDatabase = !!fileUpload;
            return fileInDatabase;
        });
    }
    fileActuallyExists(name, extension, subdir = '') {
        let fullPath = this.getFilePath(name, extension, subdir);
        let fileActuallyExists = fs.existsSync(fullPath);
        return fileActuallyExists;
    }
    createDatabaseEntry(obj) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log("creating", obj);
            let fileUpload = yield FileUpload_1.default.create(obj);
            console.log("Created");
            if (fileUpload)
                return fileUpload;
            throw new Error("Couldn't create file upload in database for some reason");
        });
    }
    getDataBaseEntry(obj) {
        return __awaiter(this, void 0, void 0, function* () {
            let fileUpload = yield FileUpload_1.default.findOne(obj);
            if (fileUpload)
                return fileUpload;
            throw new Error("Couldn't find file upload in database for some reason");
        });
    }
    getFilePath(name, extension, subdir = '') {
        return path.resolve(this.getRootDir(), 'data', subdir, name + '.' + extension);
    }
}
exports.FileManager = FileManager;
//# sourceMappingURL=FileManager.js.map