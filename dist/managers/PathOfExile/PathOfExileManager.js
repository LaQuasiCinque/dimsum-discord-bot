"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const utility_1 = require("../../helpers/utility");
const axios_1 = require("axios");
const PoEItem_1 = require("./PoEItem");
const discord_js_1 = require("discord.js");
const phantom = require("phantom");
const Manager_1 = require("../Manager");
require("../managers");
const tmp = require("tmp");
class PathOfExileManager extends Manager_1.Manager {
    constructor() {
        super(...arguments);
        this._name = 'pathOfExileManager';
    }
    install() {
        let commandManager = this.store.state.managers.commandManager;
        let self = this;
        commandManager.installManyCommands({
            unique: {
                handler: this.commandUnique.bind(this),
                description: '(Attempts (with a relatively high degree of success)) to display details about a PoE Unique',
            },
            poeitem: {
                handler: this.commandPoeItem.bind(this),
                description: '(Attempts) to format a PoE Item into an embed format'
            },
            poewiki: {
                handler: this.commandPoeWiki.bind(this),
                description: '(Attempts) to take a screenshot from the poewiki for a unique item'
            }
        });
    }
    prettyItem(item) {
        return PoEItem_1.default.parse(item.trim());
    }
    _toEmbed(itemData, wikiUrl, imgurl) {
        let poeItem = new PoEItem_1.default();
        poeItem.flavourText = this._stripHtml(this._decodeHtml(itemData.flavour_text));
        if (imgurl) {
            poeItem.image = imgurl;
        }
        poeItem.itemUrl = wikiUrl;
        poeItem.name = itemData.name;
        poeItem.requirements.level = Number(itemData.required_level);
        poeItem.requirements.strength = Number(itemData.required_strength);
        poeItem.requirements.intelligence = Number(itemData.required_intelligence);
        poeItem.requirements.dexterity = Number(itemData.required_dexterity);
        poeItem.rarity = PoEItem_1.ItemRarity.UNIQUE;
        poeItem.stats = itemData.explicit_stat_text;
        return poeItem.toDiscordRichEmbed();
    }
    _getUniques(name) {
        return axios_1.default.get(this.buildCargoQueryUrl(name)).then(response => response.data).catch(e => { console.log(name, e); });
    }
    _getUniqueNames(names) {
        let output = [];
        new Set(names)
            .forEach(item => {
            output.push(item);
        });
        return output;
    }
    _autoSearch(search) {
        return axios_1.default.get(this.api('opensearch', {
            redirects: 'resolve',
            namespace: 0,
            search,
        })).then(response => response.data).then(data => {
            return data;
        })
            .then(data => {
            return data[1].map((item, idx) => {
                return {
                    name: item,
                    url: data[3][idx]
                };
            });
        });
    }
    buildCargoQueryUrl(item) {
        let fields = [
            // 'html',
            'name',
            'flavour_text',
            'required_intelligence',
            'required_dexterity',
            'required_strength',
            'required_level',
            'explicit_stat_text',
            'inventory_icon',
            '_pageName=pageName',
        ];
        fields = fields.map(field => 'items.' + field);
        item = item.toLowerCase().replace(/'/g, "\\'").replace(/"/g, '\\"');
        let url = this.api('cargoquery', {
            tables: 'items',
            fields: fields,
            where: `items.rarity="unique" and items.name like "%${item}%"`,
            group_by: 'explicit_stat_text',
            format: 'json'
        });
        console.log(url);
        return url;
    }
    buildWikiUrl(item) {
        item = item.replace(/\s/g, '_').replace(/'/g, '%27');
        return PathOfExileManager.POEURL + item;
    }
    _addField(arr, item, name, printout, extras = {}) {
        let field = item.printouts[printout];
        if (field && field.length && field[0]) {
            arr.push(Object.assign({ name, value: field[0] }, extras));
        }
    }
    _buildApiUrl(options) {
        let keys = Object.keys(options);
        return keys.reduce((carry, value) => {
            carry.push([value, options[value]]);
            return carry;
        }, []).map(([key, value]) => {
            if (Array.isArray(value)) {
                value = value.map(item => encodeURIComponent(item)).join(',');
            }
            else {
                console.log(value);
                value = encodeURIComponent(value);
            }
            return `${key}=${value}`;
        }).join('&');
    }
    api(action, options = {}) {
        let base = PathOfExileManager.POEURL;
        options = Object.assign({ action }, options);
        return `${base}api.php?${this._buildApiUrl(options)}`;
    }
    ;
    ucfirst(str) {
        return str.charAt(0).toUpperCase() + str.substring(1).toLowerCase();
    }
    uri(str) {
        return encodeURIComponent(str).replace(/'/g, '%27');
    }
    _decodeHtml(str) {
        return str.replace(/&gt;/g, '>')
            .replace(/&quot;/g, '"')
            .replace(/&lt;/g, '<');
    }
    _countEmoji(number) {
        if (number < 0 || number > 36) {
            console.error(`Number outside of bounds (${number})`);
            throw false;
        }
        else if (number < 10) {
            return number + '⃣';
        }
        else {
            return '\ud83c' + String.fromCodePoint(56796 + number);
        }
    }
    _getIcon(file) {
        return axios_1.default.get(this.buildWikiUrl(file)).then(response => response.data)
            .then(imgdata => {
            let match = imgdata.match(/fullImageLink"[\s\S]+?href="([^\"]+?)"/);
            if (match)
                return match[1];
            else
                return null;
        });
    }
    _stripHtml(str) {
        return (str || "").replace(/<br>/gi, '\n').replace(/\<[^>]+\>/g, '');
    }
    _safeKeys(itemData) {
        return Object.keys(itemData).reduce((carry, key) => {
            carry[key.replace(/ /g, '_')] = itemData[key];
            return carry;
        }, {});
    }
    commandUnique(message, { args }) {
        if (!args.length) {
            message.react('😡');
            return;
        }
        let query = args.filter(_ => _).join(' ').trim();
        if (query.length < 3) {
            message.reply("You're a cunt. You need to include at least 3 letters.").then((message) => {
                message.react('😠');
            });
            message.react('🖕');
            return;
        }
        this._getUniques(query)
            .then(data => {
            let cargoquery = utility_1.getNested(data, 'cargoquery');
            if (!cargoquery)
                throw '😡';
            else if (cargoquery.length > 36) {
                message.react('🖕');
                message.reply(`You\'re a vague cunt aren\'t you? Please at least try to be more precise with your query. (${cargoquery.length} results found).`);
                throw false;
            }
            cargoquery = cargoquery.map(item => {
                item.title = this._safeKeys(item.title);
                item.title.explicit_stat_text = this._stripHtml(this._decodeHtml(item.title.explicit_stat_text)).split('\n');
                item.title.explicit_stat_text = PoEItem_1.default.stripMagicTags(item.title.explicit_stat_text);
                return item.title;
            });
            return cargoquery;
        }).then(cargoquery => {
            if (!cargoquery.length) {
                message.reply('No results found ya daft bat.');
                throw false;
            }
            if (cargoquery.length == 1) {
                this._getIcon(cargoquery[0].inventory_icon).then(icon => {
                    message.channel.send(this._toEmbed(cargoquery[0], this.buildWikiUrl(cargoquery[0].pageName), icon));
                });
                return;
            }
            message.channel.send(cargoquery.map((item, idx) => this._countEmoji(idx) + item.pageName).join('\n'))
                .then((msg) => {
                let accepted = [];
                let handled = false;
                cargoquery.reduce((carry, i, x) => {
                    accepted.push(this._countEmoji(x));
                    return carry.then(_ => msg.react(this._countEmoji(x)));
                }, Promise.resolve()).then(react => {
                    return msg.react('👈');
                }).then(reactions => {
                    let rc = new discord_js_1.ReactionCollector(msg, function (reaction) {
                        let wasHandled = handled;
                        let acceptableEmoji = accepted.indexOf(reaction.emoji.name) > -1;
                        handled = handled || acceptableEmoji;
                        return !wasHandled && acceptableEmoji;
                    });
                    rc.on('collect', (e) => {
                        let index = accepted.indexOf(e.emoji.name);
                        if (index == -1) {
                            return;
                        }
                        this._getIcon(cargoquery[index].inventory_icon).then(icon => {
                            msg.delete().then(oldMsg => {
                                message.channel.send(this._toEmbed(cargoquery[index], this.buildWikiUrl(cargoquery[index].pageName), icon));
                            });
                        });
                    });
                });
            });
        })
            .catch(e => { console.log(e); });
    }
    commandPoeItem(message, { args }) {
        if (!args.length)
            return;
        let item = this.prettyItem(args.join(' '));
        if (item)
            message.channel.send(item.toDiscordRichEmbed());
        else {
            console.log(item);
        }
    }
    commandPoeWiki(message, { args }) {
        if (!args)
            return;
        (function () {
            return __awaiter(this, void 0, void 0, function* () {
                const instance = yield phantom.create();
                const page = yield instance.createPage();
                page.on('onError', function () { });
                const status = yield page.open(this._buildWikiUrl(args.join('_')));
                const content = yield page.property('content');
                yield page.property('viewportSize', { width: 4000, height: 4000 });
                const rect = yield page.evaluate(function () {
                    var el = document.querySelector('.item-box.-unique');
                    var rect;
                    if (el) {
                        return el.getBoundingClientRect();
                    }
                    return null;
                });
                if (!rect)
                    throw false;
                page.property('clipRect', {
                    left: rect.left + 152,
                    top: rect.top,
                    width: rect.width + 1,
                    height: rect.height
                });
                var file = tmp.fileSync({ postfix: '.png' });
                yield page.render("test.png");
                message.reply(new discord_js_1.Attachment("test.png"));
            });
        })();
    }
}
PathOfExileManager.POEURL = 'https://pathofexile.gamepedia.com/';
exports.default = PathOfExileManager;
//# sourceMappingURL=PathOfExileManager.js.map