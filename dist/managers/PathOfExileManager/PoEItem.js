"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const discord_js_1 = require("discord.js");
var ItemRarity;
(function (ItemRarity) {
    ItemRarity[ItemRarity["NORMAL"] = 16777215] = "NORMAL";
    ItemRarity[ItemRarity["MAGIC"] = 8947967] = "MAGIC";
    ItemRarity[ItemRarity["RARE"] = 16777079] = "RARE";
    ItemRarity[ItemRarity["UNIQUE"] = 11493413] = "UNIQUE";
})(ItemRarity = exports.ItemRarity || (exports.ItemRarity = {}));
class POEItem {
    constructor() {
        this.requirements = {
            level: 0
        };
    }
    static parse(str) {
        let regex = /Rarity: (.*)\n([\s\S]+?)\n\-{8,8}\n([\s\S]+?)(?:\n\-{8,8}\nRequirements\:(?:\nLevel: (\d+))?(?:\nStr: (\d+))?(?:\nDex: (\d+))?(?:\nInt: (\d+))?)?(?:\n\-{8,8}\nSockets: (.*))?\n\-{8,8}\nItem Level: (\d+)(?:\n\-{8,8}\n(.*))?\n\-{8,8}\n([\s\S]+?$)/i;
        let matches = str.match(regex);
        if (!matches)
            return null;
        let [, rarity, nameAndBase, itemStats, lvlReq, strReq, dexReq, intReq, links, itemLevel, implicit, propertyAndFlavour] = matches;
        let [name, base] = nameAndBase.split('\n');
        let [property, flavour] = propertyAndFlavour.split('\n--------\n');
        let item = new POEItem();
        item.rarity = ItemRarity[rarity.toUpperCase()];
        if (base) {
            item.name = name;
            item.baseType = base;
        }
        else {
            item.name = base;
        }
        item.requirements.level = Number(lvlReq);
        item.requirements.strength = Number(strReq);
        item.requirements.dexterity = Number(dexReq);
        item.requirements.intelligence = Number(intReq);
        item.sockets = links;
        // item.itemLevel = Number(itemLevel)
        //item.implicit = implicit;
        item.stats = property.split('\n');
        if (flavour)
            item.flavourText = flavour;
        return item;
    }
    _stripMagicTags(stats) {
        if (typeof stats === 'string')
            return stats.replace(/\[\[([^\]]+)\]\]/g, function (_, v) {
                return v.split('|')[0];
            });
        return stats.map(stat => this._stripMagicTags(stat));
    }
    _socketToEmoji() {
        if (this.sockets) {
            let socketEmojiKeys = Object.keys(POEItem.socketEmojis);
            return this.sockets.replace(new RegExp(`[${socketEmojiKeys.join('')}]`, 'ig'), function (a) {
                return POEItem.socketEmojis[a.toUpperCase()];
            });
        }
    }
    toDiscordRichEmbed() {
        let richEmbed = new discord_js_1.RichEmbed();
        richEmbed
            .setTitle(this.name)
            .setDescription(this._stripMagicTags(this.stats).join('\n'))
            .setColor(this.rarity)
            .setThumbnail('https://d1u5p3l4wpay3k.cloudfront.net/pathofexile_gamepedia/b/bc/Wiki.png')
            .setAuthor('Path of Exile Wikia', 'https://d1u5p3l4wpay3k.cloudfront.net/pathofexile_gamepedia/b/bc/Wiki.png');
        if (this.sockets)
            richEmbed.addField('Sockets', this._socketToEmoji(), true);
        if (this.itemUrl)
            richEmbed.setURL(this.itemUrl);
        if (this.image)
            richEmbed.setImage(this.image);
        if (this.requirements.level)
            richEmbed.addField('lvl', this.requirements.level);
        if (this.requirements.strength)
            richEmbed.addField('str', this.requirements.strength, true);
        if (this.requirements.dexterity)
            richEmbed.addField('dex', this.requirements.dexterity, true);
        if (this.requirements.intelligence)
            richEmbed.addField('int', this.requirements.intelligence, true);
        if (this.flavourText)
            richEmbed.addField('--------', '```' + this.flavourText + '```');
        return richEmbed;
    }
}
POEItem.socketEmojis = {
    R: '❤',
    B: '💙',
    G: '💚',
    W: '⚪',
    A: '⚫'
};
exports.default = POEItem;
//# sourceMappingURL=POEItem.js.map