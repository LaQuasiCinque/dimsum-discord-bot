"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const discord_js_1 = require("discord.js");
const utility_1 = require("../helpers/utility");
const Manager_1 = require("./Manager");
require("./managers");
// require('webpage')
// (ffmpeg as any).setFfmpegPath(path.resolve(__dirname, '../../node_modules/ffmpeg-binaries/bin/ffmpeg.exe'));
// (ffmpeg as any).setFfprobePath(path.resolve(__dirname, '../../node_modules/ffmpeg-binaries/bin/ffprobe.exe'));
class AudioManager extends Manager_1.Manager {
    constructor() {
        super(...arguments);
        this._name = 'audioManager';
    }
    /**
     * installs the Audio Manager
     *
     * @memberof AudioManager
     */
    install() {
        let commandManager = this.managers.commandManager;
        let self = this;
        this.bot.on('ready', () => {
            this.bot.guilds.forEach(guild => {
                let voiceChannel = this.getVoiceChannel(guild);
                if (voiceChannel) {
                    voiceChannel.join().then(connection => {
                        this.leave(connection.channel);
                    });
                }
            });
        });
        let created = false;
        commandManager.installManyCommands({
            join: {
                handler: this.commandJoin.bind(this),
                description: 'Makes the bot join your current voice channel (if you\'re in one).',
            },
            leave: {
                handler: this.commandLeave.bind(this),
                description: 'Makes the bot leave the voice channel it\'s in.',
            },
            serenade: {
                handler: this.commandPlay.bind(this),
                description: '(Attempts) to play a youtube video',
            },
            shutup: {
                handler: this.commandStop.bind(this),
                description: 'Stop the bot from playing current sound',
            },
            volume: {
                handler: this.commandVolume.bind(this),
                description: 'Change the volume of the bot'
            }
        });
    }
    getUserVoiceChannel(userOrGuildMember, guild) {
        if (userOrGuildMember instanceof discord_js_1.User) {
            if (!guild)
                return null;
            userOrGuildMember = guild.member(userOrGuildMember);
        }
        return userOrGuildMember.voiceChannel;
    }
    leave(guildOrVoiceChannel) {
        let voiceChannel;
        if (guildOrVoiceChannel instanceof discord_js_1.Guild)
            voiceChannel = this.getVoiceChannel(guildOrVoiceChannel);
        else
            voiceChannel = guildOrVoiceChannel;
        if (voiceChannel) {
            voiceChannel.leave();
            return true;
        }
        return false;
    }
    playStream(guildOrConnection, stream, options) {
        let connection = null;
        if (guildOrConnection instanceof discord_js_1.Guild) {
            connection = utility_1.getNested(this.getVoiceChannel(guildOrConnection), 'connection');
        }
        else {
            connection = guildOrConnection;
        }
        if (connection && connection.playStream) {
            this.logger.info('Beginning stream');
            let self = this;
            return connection.playStream(stream, options).on('end', function (reason) {
                self.logger.info("Stream stopped because: " + reason);
            });
        }
        return null;
    }
    /**
     * Gets the current voiceChannel the bot is in
     *
     * @param {Guild} guild
     * @returns {(VoiceChannel | null)}
     * @memberof AudioManager
     */
    getVoiceChannel(guild) {
        return guild.member(this.bot.user).voiceChannel;
    }
    /**
     * Sets the default audio for things to be played
     *
     * @param {number} num
     * @memberof AudioManager
     */
    setVolume(guildResolvable, num) {
        return __awaiter(this, void 0, void 0, function* () {
            let guild = this.resolver.resolveGuild(guildResolvable);
            if (guild) {
                let guildId = guild.id;
                num = Math.max(0, Math.min(100, num));
                yield this.managers.settingsManager.setGuildSetting(guild.id, 'volume', num);
                yield this.updateDispatcher(guild);
            }
        });
    }
    /**
     * Gets the current volume
     *
     * @returns {number}
     * @memberof AudioManager
     */
    getVolume(guildResolvable) {
        return __awaiter(this, void 0, void 0, function* () {
            let guild = this.resolver.resolveGuild(guildResolvable);
            if (guild) {
                let volume = yield this.managers.settingsManager.getGuildSetting(guild.id, 'volume');
                return volume != null ? volume : 100;
            }
            this.logger.info("failed");
            return 10;
        });
    }
    /**
     * Stops the current stream for a guild (if one exists)
     *
     * @param {Guild} guild
     * @returns {boolean}
     * @memberof AudioManager
     */
    stopStream(guild) {
        let dispatcher = this.getDispatcher(guild);
        if (dispatcher) {
            dispatcher.end();
            return true;
        }
        return false;
    }
    /**
     * Gets bots stream dispatcher if it exists
     *
     * @param {Guild} guild
     * @returns {(StreamDispatcher | null)}
     * @memberof AudioManager
     */
    getDispatcher(guild) {
        return utility_1.getNested(this.getVoiceChannel(guild), 'connection.dispatcher');
    }
    updateDispatcher(guild) {
        return __awaiter(this, void 0, void 0, function* () {
            let dispatcher = this.getDispatcher(guild);
            if (dispatcher)
                dispatcher.setVolume((yield this.getVolume(guild)) / 100);
        });
    }
    playFile(guildOrConnection, filepath, streamOptions = {}) {
        let connection = null;
        if (guildOrConnection instanceof discord_js_1.Guild) {
            connection = utility_1.getNested(this.getVoiceChannel(guildOrConnection), 'connection');
        }
        else {
            connection = guildOrConnection;
        }
        if (connection && connection.playFile) {
            this.logger.info('Beginning stream');
            return connection.playFile(filepath, streamOptions);
        }
    }
    commandJoin(message) {
        let voiceChannel = this.getUserVoiceChannel(message.member);
        if (voiceChannel)
            voiceChannel.join();
    }
    commandLeave(message) {
        this.leave(message.guild);
    }
    commandPlay(message, { args }) {
        return __awaiter(this, void 0, void 0, function* () {
            if (args.length && args[0]) {
                let voiceChannel = this.getUserVoiceChannel(message.author, message.guild);
                if (voiceChannel)
                    if (voiceChannel != this.getVoiceChannel(message.guild)) {
                        let connection = yield voiceChannel.join();
                        let stream = this.managers.youtubeManager.getStream(args[0], { filter: 'audioonly' });
                        if (stream) {
                            this.playStream(message.guild, stream, { seek: 0, volume: (yield this.getVolume(message.guild)) / 100 });
                            message.react('⏹').then(reaction => {
                                let reactionCounter = new discord_js_1.ReactionCollector(message, function (item) {
                                    return item === '⏹';
                                }, { max: 1 }).on('collect', function (reaction) {
                                    this.stopStream(message.guild);
                                });
                            }).catch(reason => this.logger.error(reason));
                        }
                        else {
                            this.logger.error("No Stream D:");
                        }
                    }
                    else {
                        // TODO: DRY this
                        let stream = this.managers.youtubeManager.getStream(args[0], { filter: 'audioonly' });
                        if (stream)
                            this.playStream(message.guild, stream, { seek: 0, volume: (yield this.getVolume(message.guild)) / 100 });
                    }
            }
        });
    }
    commandStop(message) {
        this.stopStream(message.guild);
    }
    commandVolume(message, { args }) {
        return __awaiter(this, void 0, void 0, function* () {
            if (args.length && Number(args[0]))
                yield this.setVolume(message.guild, Number(args[0]));
            yield message.channel.send(`volumen postavljen na ${toCroatian(yield this.getVolume(message.guild))}😫👌 🇭🇷`);
        });
    }
}
exports.default = AudioManager;
const frame_size = 1920;
let getDecodedFrame = (frameString, encoder, filename) => {
    let buffer = Buffer.from(frameString, 'hex');
    try {
        buffer = encoder.decode(buffer, frame_size);
    }
    catch (err) {
        try {
            buffer = encoder.decode(buffer.slice(8), frame_size);
        }
        catch (err) {
            console.log(`${filename} was unable to be decoded`);
            return null;
        }
    }
    return buffer;
};
const croatian_map = [
    "nula(",
    "jedan",
    "dva",
    "tri",
    "četiri",
    "pet",
    "šest",
    "sedam",
    "osam",
    "devet",
    "deset",
    "jedanaest",
    "dvanaest",
    "trinaest",
    "cetrnaest",
    "petnaest",
    "šesnaest",
    "sedamnaest",
    "osamnaest",
    "devetnaest",
    "dvadeset",
    "dvadeset jedan",
    "dvadeset dva",
    "dvadeset tri",
    "dvadeset četiri",
    "dvadeset pet",
    "dvadeset šest",
    "dvadeset sedam",
    "dvadeset osam",
    "dvadeset devet",
    "trideset",
    "trideset jedan",
    "trideset dva",
    "trideset tri",
    "trideset četiri",
    "trideset pet",
    "trideset šest",
    "trideset sedam",
    "trideset osam",
    "trideset devet",
    "cetrdeset",
    "cetrdeset jedan",
    "cetrdeset dva",
    "cetrdeset tri",
    "cetrdeset četiri",
    "cetrdeset pet",
    "cetrdeset šest",
    "cetrdeset sedam",
    "cetrdeset osam",
    "cetrdeset devet",
    "pedeset",
    "pedeset jedan",
    "pedeset dva",
    "pedeset tri",
    "pedeset četiri",
    "pedeset pet",
    "pedeset šest",
    "pedeset sedam",
    "pedeset osam",
    "pedeset devet",
    "šezdeset",
    "šezdeset jedan",
    "šezdeset dva",
    "šezdeset tri",
    "šezdeset četiri",
    "šezdeset pet",
    "šezdeset šest",
    "šezdeset sedam",
    "šezdeset osam",
    "šezdeset devet",
    "sedamdeset",
    "sedamdeset jedan",
    "sedamdeset dva",
    "sedamdeset tri",
    "sedamdeset četiri",
    "sedamdeset pet",
    "sedamdeset šest",
    "sedamdeset sedam",
    "sedamdeset osam",
    "sedamdeset devet",
    "osamdeset",
    "osamdeset jedan",
    "osamdeset dva",
    "osamdeset tri",
    "osamdeset četiri",
    "osamdeset pet",
    "osamdeset šest",
    "osamdeset sedam",
    "osamdeset osam",
    "osamdeset devet",
    "devedeset",
    "devedeset jedan",
    "devedeset dva",
    "devedeset tri",
    "devedeset četiri",
    "devedeset pet",
    "devedeset šest",
    "devedeset sedam",
    "devedeset osam",
    "devedeset devet",
    "sto",
];
function toCroatian(num) {
    num = Math.round(Math.min(Math.max(num, 0), 100));
    return croatian_map[num];
}
//# sourceMappingURL=AudioManager.js.map