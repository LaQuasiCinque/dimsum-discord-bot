"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const env_1 = require("./modules/env");
const Bot_1 = require("./Bot");
const database_1 = require("./database");
const store_1 = require("./modules/store");
const state_1 = require("./modules/store/state");
const mutators_1 = require("./modules/store/mutators");
const AudioManager_1 = require("./managers/AudioManager");
const CommandManager_1 = require("./managers/CommandManager");
const YoutubeManager_1 = require("./managers/YoutubeManager");
const TrackingManager_1 = require("./managers/TrackingManager");
const IntroManager_1 = require("./managers/IntroManager");
const DataManager_1 = require("./managers/DataManager");
const PathOfExileManager_1 = require("./managers/PathOfExile/PathOfExileManager");
const logger_1 = require("./helpers/logger");
const SettingsManager_1 = require("./managers/SettingsManager");
const FileManager_1 = require("./managers/FileManager");
database_1.default.on('open', function () {
    logger_1.default.info("Connected to database");
});
function s(buffer) {
    let framePosition = String.prototype.indexOf.call(buffer, (new Buffer("ID3")));
    if (framePosition == -1 || framePosition > 20) {
        return -1;
    }
    else {
        return framePosition;
    }
}
const store = new store_1.default(state_1.default, mutators_1.default);
store.commit('setBot', Bot_1.bot);
const dataManager = store.commit('addManager', DataManager_1.default);
const commandManager = store.commit('addManager', CommandManager_1.default);
const settingsManager = store.commit('addManager', SettingsManager_1.SettingsManager);
const trackingManager = store.commit('addManager', TrackingManager_1.default);
const audioManager = store.commit('addManager', AudioManager_1.default);
const youtubeManager = store.commit('addManager', YoutubeManager_1.default);
const pathOfExileManager = store.commit('addManager', PathOfExileManager_1.default);
const introManager = store.commit('addManager', IntroManager_1.default);
const fileManager = store.commit('addManager', FileManager_1.FileManager);
commandManager.installCommand('purge', {
    handler(message, { args }) {
        message.channel.fetchMessages({
            limit: 100
        }).then(data => {
            let msgs = data.filter(item => {
                return item.author.id === Bot_1.bot.user.id;
            });
            Promise.all(msgs.map(item => {
                return item.delete();
            })).then(items => {
                message.react('😫').then(item => {
                    message.react('👌');
                });
            });
        }).catch(data => {
            console.error(data);
        });
    },
    description: 'Makes the bot try to delete its last few messages from the current channel.'
});
logger_1.default.info('Logging in with token ' + env_1.default.TOKEN);
Bot_1.bot.login(env_1.default.TOKEN);
const http = require('http');
const express = require('express');
const app = express();
const port = process.env.port || 3000;
app.listen(port, () => {
    console.log('Running');
});
//# sourceMappingURL=index.js.map