"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const env_1 = require("../env");
const state = {
    hook: /^(?:🔥|💯|👌(?:🏻|🏼|🏽|🏾|🏿)?|£|\$)/,
    managers: {},
    originalName: env_1.default.BOT_NAME || 'GG-Int-bot'
};
exports.default = state;
//# sourceMappingURL=state.js.map