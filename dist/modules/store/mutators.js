"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = require("../../helpers/logger");
exports.default = {
    addManager,
    setBot,
};
function addManager(store, manager) {
    let newManager = new manager(store.state.bot, store, logger_1.default);
    store.state.managers[newManager.getName()] = newManager;
    newManager.install();
    return newManager;
}
function setBot(store, bot) {
    store.state.bot = bot;
}
//# sourceMappingURL=mutators.js.map