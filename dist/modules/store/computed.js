"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const utility_1 = require("../../helpers/utility");
const computed = {};
exports.computed = computed;
const addComputed = utility_1.define(computed);
exports.addComputed = addComputed;
function removeComputed(name) {
    if (computed[name])
        delete computed[name];
}
exports.removeComputed = removeComputed;
//# sourceMappingURL=computed.js.map