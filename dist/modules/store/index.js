"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const computed_1 = require("./computed");
const utility_1 = require("../../helpers/utility");
;
class Store {
    constructor(state, mutators) {
        this.state = state;
        this._mutators = mutators;
        let _selfDefine = utility_1.define(this);
        _selfDefine('_selfDefine', {
            value: _selfDefine,
            writable: false,
            enumerable: false
        });
        _selfDefine('getters', {
            get() {
                return computed_1.computed;
            },
        });
    }
    addComputed(keyOrPairs, value) {
        if (typeof keyOrPairs === 'object') {
            for (let key in keyOrPairs) {
                if (keyOrPairs.hasOwnProperty(key)) {
                    computed_1.addComputed(key, keyOrPairs[key]);
                }
            }
        }
        else if (value)
            computed_1.addComputed(keyOrPairs, value);
    }
    removeComputed(key) {
        computed_1.removeComputed(key);
    }
    commit(name, payload) {
        if (!this._mutators[name])
            return;
        return this._mutators[name](this, payload);
    }
}
exports.default = Store;
//# sourceMappingURL=index.js.map