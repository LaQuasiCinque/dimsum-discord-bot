"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const path = require("path");
const ROOT = path.resolve(__dirname, '../..');
let env = Object.assign({}, process.env);
let envFile = path.resolve(ROOT, '.env.json');
console.log("Env File", envFile);
if (fs.existsSync(envFile)) {
    try {
        env = Object.assign({}, JSON.parse(fs.readFileSync(envFile, 'utf8')), env);
    }
    catch (e) {
        console.log("failed to parse json file");
    }
}
Object.freeze(env);
console.log(env);
exports.default = env;
//# sourceMappingURL=env.js.map