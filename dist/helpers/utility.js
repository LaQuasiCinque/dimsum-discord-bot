"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const lodash_debounce_1 = require("lodash.debounce");
exports.debounce = lodash_debounce_1.default;
exports.default = {
    getNested,
    coalesce,
    arrayContains,
    arrayPushUnique,
    arrayRemove,
    forceArray,
    define,
    objectRemove,
    isString,
    debounce: lodash_debounce_1.default,
    isUnset,
    isSet,
    promisify
};
function getNested(obj, path) {
    let argCount = arguments.length;
    if (argCount == 1 || (isUnset(path))) {
        return obj;
    }
    if (argCount == 2) {
        if (Array.isArray(arguments[1])) {
            let point = arguments[0];
            let array = arguments[1];
            for (let i = 0; i < array.length && point != null; i++) {
                point = coalesce(point[array[i]], null);
            }
            return point;
        }
        else if (isString(arguments[1])) {
            let normalizedArguments = arguments[1].replace(/\[(.*)\]/g, '.$1');
            return getNested(arguments[0], normalizedArguments.split `.`);
        }
        throw new Error("Unknown second argument provided.");
    }
    if (argCount > 2) {
        let argSlice = Array.prototype.slice.call(arguments, 1);
        return getNested(arguments[0], argSlice);
    }
    return null;
}
exports.getNested = getNested;
function coalesce(obj, nested, resultIfNull) {
    let argCount = arguments.length;
    let value;
    if (argCount === 2) {
        value = arguments[0];
        return value ? value : arguments[1];
    }
    if (argCount === 3) {
        value = getNested(arguments[0], arguments[1]);
        return coalesce(value, arguments[2]);
    }
    if (argCount > 3) {
        let argSlice = Array.prototype.slice.apply(arguments, [1, argCount - 1]);
        value = getNested(arguments[0], argSlice);
        // console.log(argSlice,value);
        return coalesce(value, arguments[argCount - 1]);
    }
    return null;
}
exports.coalesce = coalesce;
/**
 * Pushes a given value to the array but only if it doesn't already exist in the array
 *
 * @param {Array<any>} arr
 * @param {*} item
 * @returns {number}
 */
function arrayPushUnique(arr, item) {
    let index = arr.indexOf(item);
    if (index === -1) {
        return arr.push(item);
    }
    return -1;
}
exports.arrayPushUnique = arrayPushUnique;
/**
 * Removes an item from the array if it exists
 *
 * @param {Array<any>} arr
 * @param {*} item
 * @returns {(any | null)}
 */
function arrayRemove(arr, item) {
    let index = arr.indexOf(item);
    if (index !== -1) {
        return arr.splice(index, 1);
    }
    return null;
}
exports.arrayRemove = arrayRemove;
/**
 * returns whether or not a value exists within the given array
 *
 * @param {Array<any>} arr
 * @param {*} item
 * @returns {boolean}
 */
function arrayContains(arr, item) {
    return arr.indexOf(item) > -1;
}
exports.arrayContains = arrayContains;
function forceArray(obj, ...otherObj) {
    let numArgs = otherObj.length;
    if (numArgs == 0) {
        if (obj != null) {
            return Array.prototype.slice.apply(arguments);
        }
        else {
            return [];
        }
    }
    return Array.isArray(arguments[0]) ? arguments[0] : (arguments[0] === undefined) ? [] : [arguments[0]];
}
exports.forceArray = forceArray;
/**
 * Returns a function that allows for easily adding properties to first argument
 *
 * @param {object} object
 * @returns {(property: string, value: PropertyDescriptor) => void}
 */
function define(object) {
    return function (property, value) {
        return Object.defineProperty(object, property, value);
    };
}
exports.define = define;
/**
 * Removes a property from an Object
 *
 * @param {object} obj
 * @param {string} key
 */
function objectRemove(obj, key) {
    if (obj[key] !== undefined) {
        delete obj[key];
    }
}
exports.objectRemove = objectRemove;
/**
 * Returns if given object is a string
 *
 * @param {*} obj
 * @returns {obj is string}
 */
function isString(obj) {
    return typeof obj === 'string';
}
exports.isString = isString;
/**
 * returns whether the given value is null or undefined
 *
 * @param {*} item
 * @returns {boolean}
 */
function isUnset(item) {
    return item === null || item === undefined;
}
exports.isUnset = isUnset;
/**
 * returns if an item is set
 *
 * @template T
 * @param {T} item
 * @returns {item is T}
 */
function isSet(item) {
    return !isUnset(item);
}
exports.isSet = isSet;
function promisify(fn, thisArg = null) {
    let t = this;
    return function () {
        let arr = Array.prototype.slice.call(arguments);
        return new Promise((resolve, reject) => {
            let outfn = function () {
                let m = Array.prototype.slice.call(arguments);
                let err = m.shift();
                if (err)
                    return reject(err);
                return resolve(...m);
            };
            arr.push(outfn);
            fn.apply(thisArg, arr);
        });
    };
}
exports.promisify = promisify;
//# sourceMappingURL=utility.js.map