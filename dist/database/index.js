"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const env_1 = require("../modules/env");
mongoose.connect(`mongodb://${env_1.default.DB_USER}:${env_1.default.DB_PASS}@${env_1.default.DB_URI}`);
const connection = mongoose.connection;
exports.default = connection;
//# sourceMappingURL=index.js.map