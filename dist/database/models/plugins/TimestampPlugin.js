"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function timestampsPlugin(schema, options) {
    schema.add({
        createdAt: {
            type: Date,
        },
        updatedAt: {
            type: Date,
            default: Date.now
        }
    });
    schema.pre("save", function (next) {
        let now = new Date();
        if (!this.createdAt) {
            this.createdAt = now;
        }
        this.updatedAt = now;
        next();
    });
}
exports.default = timestampsPlugin;
//# sourceMappingURL=TimestampPlugin.js.map