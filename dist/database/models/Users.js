"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const logger_1 = require("../../helpers/logger");
const Schema = new mongoose.Schema({
    userId: { type: String, required: true, unique: true },
    name: { type: String, default: null },
});
Schema.statics.findOneOrCreate = function findOneOrCreate(condition, doc) {
    return __awaiter(this, void 0, void 0, function* () {
        let one;
        try {
            one = yield this.findOne(condition);
        }
        catch (e) {
            logger_1.default.error(e);
        }
        return one || this.create(doc);
    });
};
Schema.pre('validate', function (next) {
    if (!(this.createdDate)) {
        console.log("adding createdDate");
        this.createdDate = Date.now();
    }
    next();
});
let Model;
Model = mongoose.model('User', Schema, 'users');
;
exports.default = Model;
//# sourceMappingURL=Users.js.map