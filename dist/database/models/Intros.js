"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const Video = require("simple-youtube-api/src/structures/Video");
const Schema = new mongoose.Schema({
    url: {
        type: String,
        required: true,
        validate(v) {
            let id = Video.extractID(v);
            return id === undefined;
        },
        get(v) {
            return `https://www.youtube.com/watch?v=${v}`;
        },
        set(v) {
            return Video.extractID(v);
        }
    },
    volume: { type: Number, default: 0.2, max: 2 },
    start: { type: Number, default: null },
    length: { type: Number, default: null },
    createdDate: { type: Date, required: true },
    updatedDate: { type: Date, default: Date.now },
    userId: { type: String, required: true },
    fileUploadId: { type: String }
});
exports.Schema = Schema;
Schema.pre('validate', function (next) {
    if (!(this.createdDate)) {
        this.createdDate = Date.now();
    }
    next();
});
let Model;
exports.Model = Model;
exports.Model = Model = mongoose.model('Intro', Schema, 'intros');
;
exports.default = Model;
//# sourceMappingURL=Intros.js.map