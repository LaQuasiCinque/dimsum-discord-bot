"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const logger_1 = require("../../helpers/logger");
const Schema = new mongoose.Schema({
    name: { type: String, required: true, unique: true },
    path: { type: String, default: '' },
    extension: { type: String, default: '' },
    createdDate: { type: Date, required: true },
    updatedDate: { type: Date, default: Date.now },
});
exports.Schema = Schema;
Schema.pre('validate', function (next) {
    if (!(this.createdDate)) {
        this.createdDate = Date.now();
    }
    next();
});
Schema.statics.findOneOrCreate = function findOneOrCreate(condition, doc) {
    return __awaiter(this, void 0, void 0, function* () {
        let one;
        try {
            one = yield this.findOne(condition);
        }
        catch (e) {
            logger_1.default.error(e);
        }
        return one || this.create(doc);
    });
};
let FileUpload;
exports.FileUpload = FileUpload;
exports.FileUpload = FileUpload = mongoose.model('FileUpload', Schema, 'fileuploads');
;
exports.default = FileUpload;
//# sourceMappingURL=FileUpload.js.map