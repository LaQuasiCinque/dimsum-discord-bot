import * as Discord from 'discord.js';
import logger from './helpers/logger';

const bot = new Discord.Client();

bot.on('ready', function (event) {
    logger.info("Bot connected!")
    
});

bot.on('disconnect', function () {
    logger.error('Disconnected... Attempting to log in every 10 seconds')
});

export {
    bot,
}