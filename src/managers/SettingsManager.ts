import { Manager } from './Manager';
import { Message } from 'discord.js';
import GuildSetting from '../database/models/GuildSettings'

export class SettingsManager extends Manager {

    protected _name: string = 'settingsManager';

    /**
     * name
     */
    public install() {

        this.managers.commandManager.installManyCommands({
            setafk: {
                handler: this.commandAFK.bind(this),
                description: 'Tells the bot the default AFK Channel for this guild'
            },
            setbot: {
                handler: this.commandBot.bind(this),
                description: 'Tells the bot a default text channel to use (to avoid spamming the main one)'
            }
        })

    }

    /**
     * setGuildSetting
     */
    public async setGuildSetting(guild: string, settingName: string, value: any) {
        return GuildSetting.findOneOrCreate({ guildId: guild }, {
            guildId: guild
        }).then(guildSetting => {
            guildSetting.set(settingName, value)
            return guildSetting.save()
        })
    }

    public async getGuildSetting(guild: string, settingName?: string): Promise<any> {
        let settingDoc = await GuildSetting.findOne({ guildId: guild });
        let setting = settingDoc ? settingDoc.toObject() : {};
        return settingName ? setting[settingName] : setting;
    }

    private commandAFK(message: Message, { args }) {
        let fullArgument = args.join(' ');
        let guild = message.guild.id;
        let channel = this.resolver.resolveChannel(fullArgument);
        if (channel && channel.type == 'voice') {
            let channelId = channel.id;
            this.setGuildSetting(guild, 'afkChannel', channelId)
                .then(success => {
                    message.react('✅')
                }).catch(failed => {
                    console.log(failed);
                    message.react('😡')
                })
        }
        else {
            console.log("Could not find channel or channel was not voice channel")
            message.react('😡')
        }
    }

    private commandBot(message, { args }) {
        let fullArgument = args.join(' ');
        let guild = message.guild.id;
        let channel = this.resolver.resolveChannel(fullArgument);

        if (channel && channel.type == 'text') {
            let channelId = channel.id;
            this.setGuildSetting(guild, 'botChannel', channelId)
                .then(success => {
                    message.react('✅')
                }).catch(failed => {
                    console.log(failed);
                    message.react('😡')
                })
        }
        else {
            console.log("Channel was not text");
            message.react('😡')
        }
    }


}
