import env from '../modules/env';
import { Manager } from './Manager'
import * as stream from 'stream';
// import * as aws from 'aws-sdk';
import FileUpload from '../database/models/FileUpload';
// import * as request from 'request';
import * as fs from 'fs';
import { stdout as log } from 'single-line-log'
import * as path from 'path';

export class FileManager extends Manager {
    protected _name = 'fileManager';
    private _root: string;

    public async install() {
        this._root = path.resolve(__dirname, '../..')
    }

    public async uploadFile(filename: string, stream: stream.Stream, subdir: string = '') {
        return new Promise((resolve, reject) => {
            subdir = subdir.replace(/^(?:\.\.|\/)+/g, '');
            let filepath = path.resolve(this.getRootDir(), 'data', subdir, filename);
            let upload = fs.createWriteStream(filepath);
            let self = this;
            stream.pipe(upload);
            upload.on("finish", function () {
                let filenameParts = filename.split('.');
                let extension = filenameParts.length > 1 ? filenameParts.pop() : '';
                let name = filenameParts.join('.');

                resolve({
                    path: path.resolve(self.getRootDir(), 'data', subdir),
                    name,
                    extension
                });
            });
            upload.on("error", reject)
            stream.on("error", reject)
        })
    }

    public async getFileStream(filename: string, stream: stream.Stream, subdir: string): Promise<stream.Readable> {
        let fullPath: string = path.resolve(this.getRootDir(), subdir, filename);
        if (fs.existsSync(fullPath)) {
            return fs.createReadStream(fullPath);
        }
        else {
            throw new Error("File doesn't exist.")
        }
    }

    public getRootDir() {
        return this._root;
    }


    public async fileExists(name: string, extension: string, subdir: string = ''): Promise<boolean> {
        let fileActuallyExists = this.fileActuallyExists(name, extension, subdir);
        let fileInDatabase = await this.fileInDatabase(name, extension, subdir)
        return fileInDatabase && fileActuallyExists;
    }

    public async fileInDatabase(name: string, extension: string, subdir?: string) {
        let query = { name, extension }
        if (subdir) {
            query['path'] = subdir;
        }
        let fileUpload = await FileUpload.findOne(query);
        let fileInDatabase = !!fileUpload;
        return fileInDatabase
    }

    public fileActuallyExists(name: string, extension: string, subdir: string = '') {
        let fullPath = this.getFilePath(name, extension, subdir);
        let fileActuallyExists = fs.existsSync(fullPath);
        return fileActuallyExists;
    }

    public async createDatabaseEntry(obj: any) {
        console.log("creating", obj);
        let fileUpload = await FileUpload.create(obj);
        console.log("Created");
        if (fileUpload)
            return fileUpload;
        throw new Error("Couldn't create file upload in database for some reason");
    }

    public async getDataBaseEntry(obj: any) {
        let fileUpload = await FileUpload.findOne(obj);
        if (fileUpload)
            return fileUpload;
        throw new Error("Couldn't find file upload in database for some reason")
    }

    public getFilePath(name: string, extension: string, subdir: string = ''): string {
        return path.resolve(this.getRootDir(), 'data', subdir, name + '.' + extension)
    }
}