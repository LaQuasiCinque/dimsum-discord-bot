import CommandManager from "./CommandManager";
import DataManager from "./DataManager";
import AudioManager from "./AudioManager";
import IntroManager from "./IntroManager";
import TrackingManager from "./TrackingManager";
import YoutubeManager from "./YoutubeManager";
import { Manager } from "./Manager";
import { ManagerList } from "../types/interfaces";
import PathOfExileManager from "./PathOfExile/PathOfExileManager";
import { SettingsManager } from "./SettingsManager";
import { FileManager } from "./FileManager";

declare module "./Manager" {
    interface Manager {
        managers: ManagerList & {
            commandManager: CommandManager,
            dataManager: DataManager,
            audioManager: AudioManager,
            introManager: IntroManager,
            trackingManager: TrackingManager,
            youtubeManager: YoutubeManager,
            pathOfExileManager: PathOfExileManager,
            settingsManager: SettingsManager,
            fileManager: FileManager
        }
    }
}