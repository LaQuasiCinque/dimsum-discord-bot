import { Manager } from "./Manager";
import { isUnset, isString, getNested, coalesce } from "../helpers/utility";
import logger from "../helpers/logger";
import * as path from 'path';
import * as fs from 'fs';
import { Readable } from "stream";
import { WriteStream } from "fs";
import * as ffmpeg from 'fluent-ffmpeg';
import './managers';


export default class DataManager extends Manager {
    protected static BASE_PATH = path.resolve(__dirname, '../../data')

    protected _name = 'dataManager';

    public install(): void {
        logger.info("Data Path:" + DataManager.BASE_PATH);
    }


    /**
     * Writes data to a datafile
     *
     * @param {string} filepath
     * @param {(string | null)} key
     * @param {*} value
     * @returns {boolean}
     */
    public setData(filepath: string, key: string | null, value: any): boolean {
        try {
            let data = this._openDataFile(filepath);
            if (!isUnset(data)) {
                if (!this._setData(data, key, value)) {
                    return false;
                }
            }
            else {
                data = {};
                let keys = key ? key.split('.') : [];
                let lastKey = keys.pop();
                let lastObj = keys.reduce((prev, curr) => {
                    prev[curr] = {};
                    return prev[curr];
                }, {});
                // a key must be provided;
                if (lastKey) {
                    lastObj[lastKey] = value;
                }
                else {
                    console.log(arguments);
                    return false;
                }
            }
            return this._writeDataFile(filepath, data);

        } catch (e) {
            console.log(e);
            return false
        }
    }
    /**
     * Gets data from a datafile
     *
     * @param {string} filepath
     * @param {(string | null)} key
     * @returns {*}
     */
    public getData(filepath: string, key?: string): any {
        try {
            let data = this._openDataFile(filepath);
            return data ? (key ? getNested(data, key) : data) : null;
        }
        catch (e) {
            return null;
        }
    }

    /**
     * Attempts to stop people from accessing files that shouldn't be accessed
     * Try saying this with a lisp :(
     * @param filepath
     */
    private parsePath(filepath: string, extension?: string): string {
        filepath = filepath
            .replace(/^\~\./g, '')
            .replace(/[\/\\]{2,}/gi, '/')
            .replace(/\/*$/, '');

        if (extension && extension.trim()) {
            extension = extension.trim().replace(/^\.*/, '.');
            filepath = filepath + extension;
        }

        return filepath;
    }

    /**
     * Opens a data file, returns null if the file cannot be found
     *
     * @param {string} filepath
     * @returns {(object | null)}
     */
    private _openDataFile(filepath: string): object | null {
        let data: object | null = null;
        filepath = this.parsePath(filepath, '.json');
        filepath = path.resolve(DataManager.BASE_PATH, filepath);

        try {
            data = JSON.parse(fs.readFileSync(filepath, 'utf8'));
        }
        catch (e) {
            logger.info(`Failed to open ${filepath}.`)
        }
        return data;
    }

    /**
     * Writes data to a data file
     *
     * @param {string} filepath
     * @param {*} data
     * @returns {boolean}
     */
    private _writeDataFile(filepath: string, data: any): boolean {
        filepath = this.parsePath(filepath, '.json');
        filepath = path.resolve(DataManager.BASE_PATH, filepath);
        try {
            fs.writeFileSync(filepath, isUnset(data) ? null : JSON.stringify(data, null, 3));
            return true;
        }
        catch (e) {
            return false;
        }
    }

    /**
     * Sets a piece of data. Returns success
     *
     * @param {(object | null)} data
     * @param {(string | null)} key
     * @param {*} value
     * @returns {boolean}
     */
    private _setData(data: object | null, key: string | null, value: any): boolean {
        // get the keys as an array of strings (or null)
        data = data || {};
        let keyArr: Array<string> | null = isString(key) ? key.split('.') : [];
        let lastKey: string | undefined;
        let pointer: any | null = data;
        // if there is a key array, we require the last key to be our value
        if (keyArr) {
            lastKey = keyArr.pop();
        }

        for (let i = 0; i < keyArr.length; i++) {
            let key = keyArr[i];
            if (isUnset(pointer[key])) {
                pointer[key] = {};
                pointer = pointer[key];
            }
            else {
                if (pointer[key] instanceof Object) {
                    pointer = pointer[key];
                }
                else {
                    return false;
                }
            }
        }

        // if the last key exists we can now set the value;
        if (lastKey) {
            pointer[lastKey] = value;
            return true;
        }
        else {
            return false;
        }
    }


    /**
     * Writes a stream to a file location
     */
    public writeStream(filepath: string, filename: string, extension: string, stream: Readable): Promise<WriteStream> {
        return new Promise((resolve, reject) => {
            filepath = this.parsePath(filepath)
            filename = this.parsePath(filename, extension)
            let writeStream = fs.createWriteStream(path.resolve(DataManager.BASE_PATH, filepath, filename));
            writeStream.on('close', function () {
                resolve(writeStream);
            });
            stream.pipe(writeStream);
        })

    }

    /**
     * Returns if a data file already exists or not
     *
     * @param {string} filepath
     * @param {string} filename
     * @param {string} extension
     * @returns {boolean}
     * @memberof DataManager
     */
    public dataExists(filepath: string, filename: string, extension: string): boolean {
        filepath = this.parsePath(filepath);
        filename = this.parsePath(filename, extension);
        return fs.existsSync(path.resolve(DataManager.BASE_PATH, filepath, filename));
    }


    public toDataPath(filepath: string, filename: string, extension: string): string {
        filepath = this.parsePath(filepath);
        filename = this.parsePath(filename, extension);
        return path.resolve(DataManager.BASE_PATH, filepath, filename);
    }
}
