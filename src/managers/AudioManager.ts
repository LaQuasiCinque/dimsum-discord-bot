import {
    Guild,
    VoiceConnection,
    StreamDispatcher,
    User,
    GuildMember,
    GuildMemberResolvable,
    Message,
    GuildResolvable,
    StreamOptions,
    VoiceChannel,
    ReactionCollector
} from "discord.js";
import { getNested } from '../helpers/utility'
import { Readable } from "stream";
import { Manager } from "./Manager";
import CommandManager from "./CommandManager";
import './managers';

// require('webpage')

// (ffmpeg as any).setFfmpegPath(path.resolve(__dirname, '../../node_modules/ffmpeg-binaries/bin/ffmpeg.exe'));
// (ffmpeg as any).setFfprobePath(path.resolve(__dirname, '../../node_modules/ffmpeg-binaries/bin/ffprobe.exe'));

export default class AudioManager extends Manager {
    protected _name: string = 'audioManager';

    /**
     * installs the Audio Manager
     *
     * @memberof AudioManager
     */
    public install() {
        let commandManager: CommandManager = this.managers.commandManager;
        let self = this;


        this.bot.on('ready', () => {

            this.bot.guilds.forEach(guild => {
                let voiceChannel = this.getVoiceChannel(guild);
                if (voiceChannel) {
                    voiceChannel.join().then(connection => {
                        this.leave(connection.channel);
                    });
                }
            });
        });
        let created = false;
        commandManager.installManyCommands({
            join: {
                handler: this.commandJoin.bind(this),
                description: 'Makes the bot join your current voice channel (if you\'re in one).',
            },
            leave: {
                handler: this.commandLeave.bind(this),
                description: 'Makes the bot leave the voice channel it\'s in.',
            },
            serenade: {
                handler: this.commandPlay.bind(this),
                description: '(Attempts) to play a youtube video',
            },
            shutup: {
                handler: this.commandStop.bind(this),
                description: 'Stop the bot from playing current sound',
            },
            volume: {
                handler: this.commandVolume.bind(this),
                description: 'Change the volume of the bot'
            }
        })
    }

    /**
     * Gets a given user's / guild member's current voice channel, if it exists
     *
     * @param {GuildMemberResolvable} userOrGuildMember
     * @param {Guild} [guild]
     * @returns {(VoiceChannel | null)}
     * @memberof AudioManager
     */
    public getUserVoiceChannel(user: User, guild: Guild): VoiceChannel | null
    public getUserVoiceChannel(guildMember: GuildMember): VoiceChannel | null
    public getUserVoiceChannel(userOrGuildMember: GuildMemberResolvable, guild?: Guild): VoiceChannel | null {
        if (userOrGuildMember instanceof User) {
            if (!guild)
                return null;
            userOrGuildMember = guild.member(userOrGuildMember)
        }
        return userOrGuildMember.voiceChannel
    }

    /**
     * Makes the Bot leave any voice channel it is in within the guild.
     *
     * @param {Guild} guild
     * @returns {boolean}
     * @memberof AudioManager
     */
    public leave(voiceChannel: VoiceChannel): boolean;
    public leave(guild: Guild): boolean;
    public leave(guildOrVoiceChannel: Guild | VoiceChannel) {
        let voiceChannel;
        if (guildOrVoiceChannel instanceof Guild)
            voiceChannel = this.getVoiceChannel(guildOrVoiceChannel);
        else
            voiceChannel = guildOrVoiceChannel;

        if (voiceChannel) {
            voiceChannel.leave();
            return true;
        }
        return false;
    }

    /**
     * Plays a given stream to a guild if the bot is in a voice channel
     *
     * @param {VoiceConnection|Guild} guildOrConnection
     * @param {Readable} stream
     * @param {StreamOptions} [options]
     * @returns {(StreamDispatcher | null)}
     * @memberof AudioManager
     */
    public playStream(connection: VoiceConnection, stream: Readable, options?: StreamOptions): StreamDispatcher | null;
    public playStream(guild: Guild, stream: Readable, options?: StreamOptions): StreamDispatcher | null;
    public playStream(guildOrConnection: (VoiceConnection | Guild), stream: Readable, options?: StreamOptions): StreamDispatcher | null {
        let connection: VoiceConnection | null = null;

        if (guildOrConnection instanceof Guild) {
            connection = getNested(this.getVoiceChannel(guildOrConnection), 'connection');
        }
        else {
            connection = guildOrConnection;
        }

        if (connection && connection.playStream) {
            this.logger.info('Beginning stream')
            let self = this;
            return connection.playStream(stream, options).on('end', function (reason) {
                self.logger.info("Stream stopped because: " + reason);
            });
        }
        return null;
    }

    /**
     * Gets the current voiceChannel the bot is in
     *
     * @param {Guild} guild
     * @returns {(VoiceChannel | null)}
     * @memberof AudioManager
     */
    public getVoiceChannel(guild: Guild): VoiceChannel | null {
        return guild.member(this.bot.user).voiceChannel;
    }

    /**
     * Sets the default audio for things to be played
     *
     * @param {number} num
     * @memberof AudioManager
     */
    public async setVolume(guildResolvable: GuildResolvable, num: number): Promise<void> {
        let guild = this.resolver.resolveGuild(guildResolvable);
        if (guild) {
            let guildId = guild.id;
            num = Math.max(0, Math.min(100, num));
            await this.managers.settingsManager.setGuildSetting(guild.id, 'volume', num);

            await this.updateDispatcher(guild);
        }

    }

    /**
     * Gets the current volume
     *
     * @returns {number}
     * @memberof AudioManager
     */
    public async getVolume(guildResolvable: GuildResolvable): Promise<number> {
        let guild = this.resolver.resolveGuild(guildResolvable);
        if (guild) {
            let volume = await this.managers.settingsManager.getGuildSetting(guild.id, 'volume');
            return volume != null ? volume : 100;
        }
        this.logger.info("failed");
        return 10;
    }

    /**
     * Stops the current stream for a guild (if one exists)
     *
     * @param {Guild} guild
     * @returns {boolean}
     * @memberof AudioManager
     */
    public stopStream(guild: Guild): boolean {
        let dispatcher: StreamDispatcher | null = this.getDispatcher(guild);
        if (dispatcher) {
            dispatcher.end();
            return true;
        }
        return false;
    }

    /**
     * Gets bots stream dispatcher if it exists
     *
     * @param {Guild} guild
     * @returns {(StreamDispatcher | null)}
     * @memberof AudioManager
     */
    public getDispatcher(guild: Guild): StreamDispatcher | null {
        return getNested(this.getVoiceChannel(guild), 'connection.dispatcher');
    }

    private async updateDispatcher(guild: Guild) {
        let dispatcher = this.getDispatcher(guild);
        if (dispatcher)
            dispatcher.setVolume(await this.getVolume(guild) / 100);
    }

    public playFile(guildOrConnection: Guild | VoiceConnection, filepath: string, streamOptions: StreamOptions = {}) {
        let connection: VoiceConnection | null = null;

        if (guildOrConnection instanceof Guild) {
            connection = getNested(this.getVoiceChannel(guildOrConnection), 'connection');
        }
        else {
            connection = guildOrConnection;
        }

        if (connection && connection.playFile) {
            this.logger.info('Beginning stream')
            return connection.playFile(filepath, streamOptions);
        }
    }

    private commandJoin(message: Message) {
        let voiceChannel = this.getUserVoiceChannel(message.member);
        if (voiceChannel)
            voiceChannel.join()
    }

    private commandLeave(message: Message) {
        this.leave(message.guild);
    }

    private async commandPlay(message: Message, { args }) {
        if (args.length && args[0]) {
            let voiceChannel = this.getUserVoiceChannel(message.author, message.guild);

            if (voiceChannel)
                if (voiceChannel != this.getVoiceChannel(message.guild)) {
                    let connection = await voiceChannel.join()

                    let stream = this.managers.youtubeManager.getStream(args[0], { filter: 'audioonly' });
                    if (stream) {
                        this.playStream(message.guild, stream, { seek: 0, volume: await this.getVolume(message.guild) / 100 });
                        message.react('⏹').then(reaction => {
                            let reactionCounter = new ReactionCollector(message, function (item) {
                                return item === '⏹';
                            }, { max: 1 }).on('collect', function (reaction) {
                                this.stopStream(message.guild);
                            })
                        }).catch(reason => this.logger.error(reason))
                    }
                    else {
                        this.logger.error("No Stream D:");
                    }
                }
                else {
                    // TODO: DRY this
                    let stream = this.managers.youtubeManager.getStream(args[0], { filter: 'audioonly' });
                    if (stream)
                        this.playStream(message.guild, stream, { seek: 0, volume: await this.getVolume(message.guild) / 100 });
                }
        }
    }

    private commandStop(message: Message) {
        this.stopStream(message.guild)
    }

    private async commandVolume(message: Message, { args }) {
        if (args.length && Number(args[0]))
            await this.setVolume(message.guild, Number(args[0]))
        await message.channel.send(`volumen postavljen na ${toCroatian(await this.getVolume(message.guild))}😫👌 🇭🇷`);
    }
}

const frame_size = 1920;
let getDecodedFrame = (frameString, encoder, filename) => {
    let buffer = Buffer.from(frameString, 'hex');
    try {
        buffer = encoder.decode(buffer, frame_size);
    } catch (err) {
        try {
            buffer = encoder.decode(buffer.slice(8), frame_size);
        } catch (err) {
            console.log(`${filename} was unable to be decoded`);
            return null;
        }
    }
    return buffer;
};


const croatian_map = [
    "nula(",
    "jedan",
    "dva",
    "tri",
    "četiri",
    "pet",
    "šest",
    "sedam",
    "osam",
    "devet",
    "deset",
    "jedanaest",
    "dvanaest",
    "trinaest",
    "cetrnaest",
    "petnaest",
    "šesnaest",
    "sedamnaest",
    "osamnaest",
    "devetnaest",
    "dvadeset",
    "dvadeset jedan",
    "dvadeset dva",
    "dvadeset tri",
    "dvadeset četiri",
    "dvadeset pet",
    "dvadeset šest",
    "dvadeset sedam",
    "dvadeset osam",
    "dvadeset devet",
    "trideset",
    "trideset jedan",
    "trideset dva",
    "trideset tri",
    "trideset četiri",
    "trideset pet",
    "trideset šest",
    "trideset sedam",
    "trideset osam",
    "trideset devet",
    "cetrdeset",
    "cetrdeset jedan",
    "cetrdeset dva",
    "cetrdeset tri",
    "cetrdeset četiri",
    "cetrdeset pet",
    "cetrdeset šest",
    "cetrdeset sedam",
    "cetrdeset osam",
    "cetrdeset devet",
    "pedeset",
    "pedeset jedan",
    "pedeset dva",
    "pedeset tri",
    "pedeset četiri",
    "pedeset pet",
    "pedeset šest",
    "pedeset sedam",
    "pedeset osam",
    "pedeset devet",
    "šezdeset",
    "šezdeset jedan",
    "šezdeset dva",
    "šezdeset tri",
    "šezdeset četiri",
    "šezdeset pet",
    "šezdeset šest",
    "šezdeset sedam",
    "šezdeset osam",
    "šezdeset devet",
    "sedamdeset",
    "sedamdeset jedan",
    "sedamdeset dva",
    "sedamdeset tri",
    "sedamdeset četiri",
    "sedamdeset pet",
    "sedamdeset šest",
    "sedamdeset sedam",
    "sedamdeset osam",
    "sedamdeset devet",
    "osamdeset",
    "osamdeset jedan",
    "osamdeset dva",
    "osamdeset tri",
    "osamdeset četiri",
    "osamdeset pet",
    "osamdeset šest",
    "osamdeset sedam",
    "osamdeset osam",
    "osamdeset devet",
    "devedeset",
    "devedeset jedan",
    "devedeset dva",
    "devedeset tri",
    "devedeset četiri",
    "devedeset pet",
    "devedeset šest",
    "devedeset sedam",
    "devedeset osam",
    "devedeset devet",
    "sto",
]

function toCroatian(num: number) {
    num = Math.round(Math.min(Math.max(num, 0), 100))
    return croatian_map[num]
}
