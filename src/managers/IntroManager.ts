import { Manager } from "./Manager";
import { GuildMember, UserResolvable, User, Client, VoiceConnection, VoiceChannel, VoiceBroadcast, Message, StreamDispatcher } from "discord.js";
import YoutubeManager from "./YoutubeManager";
import AudioManager from "./AudioManager";
import { isUnset, isSet, coalesce } from "../helpers/utility";
import { downloadOptions } from "ytdl-core";
import DataManager from "./DataManager";
import { ManagerList } from "../types/interfaces";
import './managers';
import { lchmod } from "fs";

import Intro from '../database/models/Intros'
import { Document } from "mongoose";
import FileUpload, { FileUploadType } from "../database/models/FileUpload";

export interface IntroData {
    url: string,
    volume: number,
    oldVolume?: number,
    length?: number,
    start?: number,
    history?: Array<IntroData>,
    lastPlayed?: number,
    fileUploadId?: string,
}

export default class IntroManager extends Manager {
    public static TIME_LIMIT: number = 0;
    protected _name = 'introManager';
    private _num = 0;
    private _temp = false;
    private _introsLoaded = false;

    public async install() {

        this.downloadMissingIntros();

        this.bot.on('voiceStateUpdate', (oldMember: GuildMember, newMember: GuildMember) => {
            try {
                if (this._introsLoaded) {
                    this.playIntro(oldMember, newMember)
                }
            } catch (e) {
                this.logger.error(e);
            }
            if (oldMember.voiceChannel && !newMember.voiceChannel) {
                let voiceChannel = oldMember.voiceChannel;
                let selfAsGM = this.resolver.resolveGuildMember(oldMember.guild, this.bot.user);
                let numberOfOtherUsers = voiceChannel.members.filter(gm => {
                    return gm.id != selfAsGM.id;
                }).size;

                if (numberOfOtherUsers == 0 && selfAsGM.voiceChannel == oldMember.voiceChannel) {
                    this.managers.audioManager.leave(voiceChannel);
                }
            }
        });

        this.managers.commandManager.installManyCommands({
            'intro': {
                handler: this.commandPlayIntro.bind(this),
                description: 'Plays the intro for a user'
            },
            'changeintro': {
                handler: this.commandChangeIntro.bind(this),
                description: 'Change your intro. arguments\n'
                    + '`<youtubeurl> [volume=0.2] [length=null] [begin=0]`'
            }
        })
    }

    public async downloadMissingIntros() {
        let data = await Intro.aggregate([
            { $sort: { 'updatedDate': -1 } },
            {
                $group: {
                    _id: '$userId',
                    id: { $first: '$_id' },
                    youtubeId: { $first: '$url' },
                    fileUploadId: { $first: "$fileUploadId" }
                }
            }
        ]);
        let urls = data.map(item => {
            item.userId = item._id;
            item.introId = item.id;
            item.fullUrl = `https://www.youtube.com/watch?v=${item.youtubeId}`;
            delete item._id;
            delete item.id;
            return item;
        });


        let fm = this.managers.fileManager;
        let introData = await Promise.all(urls.map(async (item, index) => {
            let file;
            let fileUpload = await this.managers.youtubeManager.downloadStream(item.fullUrl);
            item.fileUpload = fileUpload;
            return item;
        }));

        introData = introData.filter(item => {
            return item.fileUpload._id !== item.fileUploadId;
        })

        await Promise.all(introData.map(async (item) => {
            await Intro.findByIdAndUpdate(item.introId, {
                fileUploadId: item.fileUpload._id
            })
            console.log("intro attached");
            return true;
        }))

        this._introsLoaded = true;
    }

    public async getUserIntroData(user: UserResolvable): Promise<IntroData | null> {
        let userId: string = this.resolver.resolveUserID(user);
        let data = await Intro.find({ 'userId': userId })
            .sort({ updatedDate: -1 })
            .limit(1)
            .exec()

        return data.length ? data[0].toObject() : null
    }

    public async playIntro(oldMember: GuildMember | null, newMember: GuildMember) {
        let bot = this.bot;
        let self = this;
        let guild = newMember.guild;
        let selfGuildMember = this.resolver.resolveGuildMember(guild, bot.user);

        // if the user didn't mvoe to the afk channel
        if (newMember.voiceChannelID === await this.managers.settingsManager.getGuildSetting(guild.id, 'afkChannel'))
            return;
        // if the user didnt change channels
        if (oldMember && oldMember.voiceChannelID === newMember.voiceChannelID)
            return;
        // if there is no one else in the voice channel except the bot and the current user
        if (this.voiceChannelIsEmpty(newMember.voiceChannel, [
            selfGuildMember.id,
            newMember.id
        ])) {
            return;
        }
        let intro: IntroData = await this.getUserIntroData(newMember.user) as IntroData;
        let youtubeManager = this.managers.youtubeManager;
        let audioManager = this.managers.audioManager;

        if (intro != null) {
            if (newMember.voiceChannel) {
                let lastPlayed = new Date(intro.lastPlayed || 0).getTime();
                let now = new Date().getTime();
                let currNum = ++this._num;
                let connection: VoiceConnection = await this._joinNewMemberChannel(newMember)

                if (!intro.url) {
                    throw '😢';
                }

                let file = await FileUpload.findById(intro.fileUploadId) as Document & FileUploadType;
                if (!file)
                    throw '😡';

                let dispatcher = this.managers.audioManager.playFile(connection, this.managers.fileManager.getFilePath(file.name, file.extension, file.path), { seek: coalesce(intro, 'start', 0), volume: intro.volume || 0.2 })
                if (!dispatcher)
                    throw '😫';

                dispatcher.on('end', function () {
                    self.logger.info("finished");
                });

                if (intro.length) {
                    dispatcher.on('start', function () {
                        let length: number = intro.length ? intro.length : 0;
                        setTimeout(function () {
                            if (dispatcher)
                                dispatcher.end();
                        }, 1000 * length);
                    })
                }
            };
        }
    }


    private _joinNewMemberChannel(newMember: GuildMember): Promise<VoiceConnection> {
        let bot: Client = this.bot;
        let audioManager = this.managers.audioManager;
        let botVoiceChannel = audioManager.getVoiceChannel(newMember.guild)

        if (newMember.voiceChannel == botVoiceChannel) {
            return Promise.resolve(botVoiceChannel.connection)
        }
        else if (botVoiceChannel) {
            audioManager.leave(newMember.guild)
        }
        return newMember.voiceChannel.join()
    }


    private _toTimeFormat(num = 0) {
        let minutes = ("00" + Math.floor(num / 60)).substr(-2);
        let seconds = ("00" + (num % 60)).substr(-2);
        return `${minutes}:${seconds}`;
    }

    private commandPlayIntro(message, { args }) {
        if (!this._introsLoaded)
            return;
        if (!args.length)
            return;
        let user = message.guild.members.find((guildMember: GuildMember) => {
            return guildMember.user.username.toLowerCase() == args[0].toLowerCase()
        })

        if (!user)
            return;

        let messager = this.resolver.resolveGuildMember(message.guild, message.author);
        user.voiceChannelID = messager.voiceChannelID;

        this.playIntro(null, user);
    }

    private async commandChangeIntro(message: Message, { args }) {
        if (!args.length)
            return;
        let userId: string = this.resolver.resolveUserID(message);
        let [
            url,
            volume = 0.2,
            length = null,
            start = 0,
        ] = args;
        volume = Math.max(Math.min(volume, 0.2), 0);
        length = length == null ? 15 : Math.max(0, Math.min(length, 15));
        start = Math.max(0, start);
        let download: Document;
        let reaction;
        try {
            reaction = await message.react('🕐');
            download = await this.managers.youtubeManager.downloadStream(url, { filter: 'audioonly' })

            let newIntro = await Intro.create({
                url,
                volume,
                start,
                length,
                fileUploadId: download._id,
                userId
            });
            if (reaction) {
                await reaction.remove()
                await message.react('✅')
            }


        }
        catch (error) {
            if (reaction) {
                await reaction.remove()
            }
            console.log(error);
            await message.react('😡')
        }




    }

    private voiceChannelIsEmpty(voiceChannel: VoiceChannel, exclude: Array<string> = []) {
        if (!voiceChannel)
            return;
        let members = voiceChannel.members.filter((member: GuildMember) => {
            return exclude.indexOf(member.id) > -1;
        });
        return members.size == 0
    }
}
