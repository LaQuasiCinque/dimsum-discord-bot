import { RichEmbed } from 'discord.js'
import { isString } from '../../helpers/utility';

export enum ItemRarity {
    NORMAL = 0xFFFFFF,
    MAGIC = 0x8888FF,
    RARE = 0xFFFF77,
    UNIQUE = 0xAF6025
}

export interface ItemRequirements {
    level: number,
    strength?: number,
    intelligence?: number,
    dexterity?: number,
}

export default class POEItem {
    private static socketEmojis = {
        R: '❤',
        B: '💙',
        G: '💚',
        W: '⚪',
        A: '⚫'
    }
    public name: string;
    public baseType: string;
    public classType: string;
    public flavourText: string;
    public rarity: ItemRarity
    public stats: Array<string>;
    public requirements: ItemRequirements = {
        level: 0
    };
    public url?: string;
    public itemUrl?: string;
    public image?: string;
    public sockets?: string;

    constructor() {

    }

    public static parse(str: string): POEItem | null {
        let regex = /Rarity: (.*)\n([\s\S]+?)\n\-{8,8}\n([\s\S]+?)(?:\n\-{8,8}\nRequirements\:(?:\nLevel: (\d+))?(?:\nStr: (\d+))?(?:\nDex: (\d+))?(?:\nInt: (\d+))?)?(?:\n\-{8,8}\nSockets: (.*))?\n\-{8,8}\nItem Level: (\d+)(?:\n\-{8,8}\n(.*))?\n\-{8,8}\n([\s\S]+?$)/i
        let matches = str.match(regex);
        if (!matches)
            return null;

        let [,
            rarity,
            nameAndBase,
            itemStats,
            lvlReq,
            strReq,
            dexReq,
            intReq,
            links,
            itemLevel,
            implicit,
            propertyAndFlavour
        ] = matches;

        let [name, base] = nameAndBase.split('\n')
        let [property, flavour] = propertyAndFlavour.split('\n--------\n');

        let item = new POEItem();
        item.rarity = ItemRarity[rarity.toUpperCase()];
        if (base) {
            item.name = name;
            item.baseType = base;
        }
        else {
            item.name = base;
        }
        item.requirements.level = Number(lvlReq);
        item.requirements.strength = Number(strReq);
        item.requirements.dexterity = Number(dexReq);
        item.requirements.intelligence = Number(intReq);
        item.sockets = links;
        // item.itemLevel = Number(itemLevel)
        //item.implicit = implicit;
        item.stats = property.split('\n');
        if (flavour)
            item.flavourText = flavour;

        return item;
    }

    public stripMagicTags(stats: Array<string>): Array<string>;
    public stripMagicTags(stats: string): string;
    public stripMagicTags(stats: Array<string> | string): Array<string> | string {
        if (isString(stats))
            return POEItem.stripMagicTags(stats);
        else
            return POEItem.stripMagicTags(stats);
    }


    public static stripMagicTags(stats: Array<string>): Array<string>;
    public static stripMagicTags(stats: string): string;
    public static stripMagicTags(stats: Array<string> | string): Array<string> | string {
        if (isString(stats))
            return stats.replace(/\[\[([^\]]+)\]\]/g, function (_, v) {
                return v.split('|')[0]
            })

        return stats.map(stat => this.stripMagicTags(stat))
    }

    private _socketToEmoji() {
        if (this.sockets) {
            let socketEmojiKeys = Object.keys(POEItem.socketEmojis)
            return this.sockets.replace(new RegExp(`[${socketEmojiKeys.join('')}]`, 'ig'), function (a) {
                return POEItem.socketEmojis[a.toUpperCase()]
            });
        }
    }

    public toDiscordRichEmbed(): RichEmbed {
        let richEmbed: RichEmbed = new RichEmbed();
        richEmbed
            .setTitle(this.name)
            .setDescription(this.stripMagicTags(this.stats).join('\n'))
            .setColor(this.rarity)
            .setThumbnail('https://d1u5p3l4wpay3k.cloudfront.net/pathofexile_gamepedia/b/bc/Wiki.png')
            .setAuthor('Path of Exile Wikia', 'https://d1u5p3l4wpay3k.cloudfront.net/pathofexile_gamepedia/b/bc/Wiki.png');
        if (this.sockets)
            richEmbed.addField('Sockets', this._socketToEmoji(), true);
        if (this.itemUrl)
            richEmbed.setURL(this.itemUrl);
        if (this.image)
            richEmbed.setImage(this.image);
        if (this.requirements.level)
            richEmbed.addField('lvl', this.requirements.level);
        if (this.requirements.strength)
            richEmbed.addField('str', this.requirements.strength, true)
        if (this.requirements.dexterity)
            richEmbed.addField('dex', this.requirements.dexterity, true)
        if (this.requirements.intelligence)
            richEmbed.addField('int', this.requirements.intelligence, true)
        if (this.flavourText)
            richEmbed.addField('--------', '```' + this.flavourText + '```');
        return richEmbed;
    }
}