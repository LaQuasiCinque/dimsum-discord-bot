import { getNested, isString } from "../../helpers/utility";
import axios from 'axios'
import POEItem, { ItemRarity } from "./PoEItem";
import { RichEmbed, Attachment, Message, ReactionCollector } from "discord.js";
import CommandManager from "../CommandManager";
import phantom = require('phantom');
import { Buffer } from "buffer";
import { Manager } from "../Manager";
import '../managers';

import tmp = require('tmp');




export default class PathOfExileManager extends Manager {

    protected _name: string = 'pathOfExileManager';

    public static POEURL = 'https://pathofexile.gamepedia.com/';

    public install() {
        let commandManager = (this.store.state.managers.commandManager as CommandManager);
        let self = this;
        commandManager.installManyCommands({
            unique: {
                handler: this.commandUnique.bind(this),
                description: '(Attempts (with a relatively high degree of success)) to display details about a PoE Unique',
            },
            poeitem: {
                handler: this.commandPoeItem.bind(this),
                description: '(Attempts) to format a PoE Item into an embed format'
            },
            poewiki: {
                handler: this.commandPoeWiki.bind(this),
                description: '(Attempts) to take a screenshot from the poewiki for a unique item'
            }
        });
    }

    public prettyItem(item: string) {
        return POEItem.parse(item.trim());
    }

    private _toEmbed(itemData, wikiUrl?, imgurl?) {

        let poeItem = new POEItem();
        poeItem.flavourText = this._stripHtml(this._decodeHtml(itemData.flavour_text));
        if (imgurl) {
            poeItem.image = imgurl;
        }
        poeItem.itemUrl = wikiUrl
        poeItem.name = itemData.name;
        poeItem.requirements.level = Number(itemData.required_level)
        poeItem.requirements.strength = Number(itemData.required_strength);
        poeItem.requirements.intelligence = Number(itemData.required_intelligence);
        poeItem.requirements.dexterity = Number(itemData.required_dexterity);
        poeItem.rarity = ItemRarity.UNIQUE
        poeItem.stats = itemData.explicit_stat_text


        return poeItem.toDiscordRichEmbed();
    }

    private _getUniques(name: string): Promise<any> {
        return axios.get(this.buildCargoQueryUrl(name)).then(response => response.data).catch(e => { console.log(name, e) });
    }

    private _getUniqueNames(names: Array<String>): Array<String> {
        let output: Array<String> = [];
        new Set(names)
            .forEach(item => {
                output.push(item);
            })
        return output;
    }

    private _autoSearch(search: String) {
        return axios.get(this.api('opensearch', {
            redirects: 'resolve',
            namespace: 0,
            search,
        })).then(response => response.data).then(data => {
            return data;
        })
            .then(data => {
                return data[1].map((item, idx) => {
                    return {
                        name: item,
                        url: data[3][idx]
                    }
                })
            })
    }

    public buildCargoQueryUrl(item) {
        let fields = [
            // 'html',
            'name',
            'flavour_text',
            'required_intelligence',
            'required_dexterity',
            'required_strength',
            'required_level',
            'explicit_stat_text',
            'inventory_icon',
            '_pageName=pageName',
        ];
        fields = fields.map(field => 'items.' + field);
        item = item.toLowerCase().replace(/'/g, "\\'").replace(/"/g, '\\"');
        let url = this.api('cargoquery', {
            tables: 'items',
            fields: fields,
            where: `items.rarity="unique" and items.name like "%${item}%"`,
            group_by: 'explicit_stat_text',
            format: 'json'
        });
        console.log(url);
        return url;
    }

    public buildWikiUrl(item) {
        item = item.replace(/\s/g, '_').replace(/'/g, '%27')
        return PathOfExileManager.POEURL + item;
    }

    private _addField(arr, item, name, printout, extras = {}) {
        let field = item.printouts[printout];
        if (field && field.length && field[0]) {
            arr.push({
                name,
                value: field[0],
                ...extras,
            })
        }
    }

    private _buildApiUrl(options: object) {
        let keys = Object.keys(options);
        return keys.reduce((carry: Array<any>, value: string) => {
            carry.push([value, options[value]]);
            return carry;
        }, []).map(([key, value]) => {
            if (Array.isArray(value)) {
                value = value.map(item => encodeURIComponent(item)).join(',');
            }
            else {
                console.log(value);
                value = encodeURIComponent(value);
            }

            return `${key}=${value}`;
        }).join('&');
    }

    private api(action, options = {}) {
        let base = PathOfExileManager.POEURL;
        options = { action, ...options }
        return `${base}api.php?${this._buildApiUrl(options)}`;
    };

    private ucfirst(str) {
        return str.charAt(0).toUpperCase() + str.substring(1).toLowerCase();
    }

    private uri(str) {
        return encodeURIComponent(str).replace(/'/g, '%27');
    }

    private _decodeHtml(str: string) {
        return str.replace(/&gt;/g, '>')
            .replace(/&quot;/g, '"')
            .replace(/&lt;/g, '<')

    }

    private _countEmoji(number: number) {
        if (number < 0 || number > 36) {
            console.error(`Number outside of bounds (${number})`);
            throw false;
        }
        else if (number < 10) {
            return number + '⃣'
        }
        else {
            return '\ud83c' + String.fromCodePoint(56796 + number);
        }
    }

    private _getIcon(file) {
        return axios.get(this.buildWikiUrl(file)).then(response => response.data)
            .then(imgdata => {
                let match = imgdata.match(/fullImageLink"[\s\S]+?href="([^\"]+?)"/);
                if (match)
                    return match[1];
                else
                    return null;
            });
    }

    private _stripHtml(str: string) {
        return (str || "").replace(/<br>/gi, '\n').replace(/\<[^>]+\>/g, '');
    }

    private _safeKeys(itemData) {
        return Object.keys(itemData).reduce((carry, key) => {
            carry[key.replace(/ /g, '_')] = itemData[key];
            return carry;
        }, {});
    }

    private commandUnique(message, { args }) {
        if (!args.length) {
            message.react('😡')
            return;
        }
        let query = args.filter(_ => _).join(' ').trim();

        if (query.length < 3) {
            message.reply("You're a cunt. You need to include at least 3 letters.").then((message: Message) => {
                message.react('😠')
            })
            message.react('🖕')
            return;
        }

        this._getUniques(query)
            .then(data => {
                let cargoquery = getNested(data, 'cargoquery');
                if (!cargoquery)
                    throw '😡';
                else if (cargoquery.length > 36) {
                    message.react('🖕');
                    message.reply(`You\'re a vague cunt aren\'t you? Please at least try to be more precise with your query. (${cargoquery.length} results found).`)
                    throw false;
                }

                cargoquery = cargoquery.map(item => {
                    item.title = this._safeKeys(item.title);
                    item.title.explicit_stat_text = this._stripHtml(this._decodeHtml(item.title.explicit_stat_text)).split('\n')
                    item.title.explicit_stat_text = POEItem.stripMagicTags(item.title.explicit_stat_text);
                    return item.title;
                });
                return cargoquery
            }).then(cargoquery => {
                if (!cargoquery.length) {
                    message.reply('No results found ya daft bat.');
                    throw false;
                }
                if (cargoquery.length == 1) {
                    this._getIcon(cargoquery[0].inventory_icon).then(icon => {
                        message.channel.send(this._toEmbed(cargoquery[0], this.buildWikiUrl(cargoquery[0].pageName), icon))
                    })
                    return;
                }
                message.channel.send(cargoquery.map((item, idx) => this._countEmoji(idx) + item.pageName).join('\n'))
                    .then((msg: Message) => {
                        let accepted: Array<string> = [];
                        let handled = false;

                        cargoquery.reduce((carry, i, x) => {
                            accepted.push(this._countEmoji(x));
                            return carry.then(_ => msg.react(this._countEmoji(x)));
                        }, Promise.resolve()).then(react => {
                            return msg.react('👈')
                        }).then(reactions => {
                            let rc = new ReactionCollector(msg, function (reaction) {
                                let wasHandled = handled;
                                let acceptableEmoji = accepted.indexOf(reaction.emoji.name) > -1;
                                handled = handled || acceptableEmoji;
                                return !wasHandled && acceptableEmoji
                            });

                            rc.on('collect', (e) => {
                                let index = accepted.indexOf(e.emoji.name);
                                if (index == -1) {
                                    return;
                                }
                                this._getIcon(cargoquery[index].inventory_icon).then(icon => {
                                    msg.delete().then(oldMsg => {
                                        message.channel.send(this._toEmbed(cargoquery[index], this.buildWikiUrl(cargoquery[index].pageName), icon))
                                    })
                                })
                            });
                        })
                    });
            })
            .catch(e => { console.log(e) })
    }

    private commandPoeItem(message, { args }) {
        if (!args.length)
            return;
        let item = this.prettyItem(args.join(' '))
        if (item)
            message.channel.send(item.toDiscordRichEmbed());
        else {
            console.log(item);

        }
    }

    private commandPoeWiki(message, { args }) {
        if (!args)
            return;
        (async function () {
            const instance = await phantom.create();
            const page = await instance.createPage();

            page.on('onError', function () { })

            const status = await page.open(this._buildWikiUrl(args.join('_')));
            const content = await page.property('content');

            await page.property('viewportSize', { width: 4000, height: 4000 })


            const rect = await page.evaluate(function () {
                var el = document.querySelector('.item-box.-unique');
                var rect;
                if (el) {
                    return el.getBoundingClientRect();
                }
                return null;
            });

            if (!rect)
                throw false;

            page.property('clipRect', {
                left: rect.left + 152,
                top: rect.top,
                width: rect.width + 1,
                height: rect.height
            });
            var file = tmp.fileSync({ postfix: '.png' });
            await page.render("test.png")
            message.reply(new Attachment("test.png"))


        })();
    }

}
