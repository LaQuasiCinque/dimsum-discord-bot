import * as ytdl from 'ytdl-core';
import { Manager } from './Manager';
import * as Youtube from 'simple-youtube-api';
import * as Video from 'simple-youtube-api/src/structures/Video';
import { Readable } from 'stream';
import * as fs from 'fs';
import { ManagerList } from '../types/interfaces';
import { WriteStream } from 'fs';
import './managers';
import FileUpload from '../database/models/FileUpload';
import { Model, Document } from 'mongoose';

export default class YoutubeManager extends Manager {
    protected _name: string = 'youtubeManager';
    protected _queue: Array<string> = [];

    /**
     * install
     */
    public install() {
        this.managers.commandManager.installCommand('dl', {
            handler: this.commandDownload.bind(this),
            description: 'Downloads a Youtube video, Adam only >:('
        })
    }

    public async downloadStream(link: string, options: ytdl.downloadOptions = { filter: 'audioonly' }): Promise<Document> {
        let id = this.getId(link);
        let name = `youtube-${id}`;
        let extension = options.filter == 'audioonly' ? 'mp3' : 'mp4';
        let fm = this.managers.fileManager;
        // file exists both as a file and database collection
        if (await this.managers.fileManager.fileExists(name, extension)) {
            console.log("File exists 100%")
            return await fm.getDataBaseEntry({ name, extension });
        }

        // file exists but no database entry
        if (await this.managers.fileManager.fileActuallyExists(name, extension)) {
            console.log("file not found in database but exists")
            return await fm.createDatabaseEntry({ name, extension, path: '' });
        }

        // Here the file doesn't exist in data
        let stream = this.getStream(link, options);
        if (stream) {
            let fileUploadData = await this.managers.fileManager.uploadFile(name + '.' + extension, stream);
            if (!(await this.managers.fileManager.fileInDatabase(name, extension))) {
                console.log("file not in database and now exists")
                return await fm.createDatabaseEntry(fileUploadData)
            }
            console.log("file is in database but also exists")
            return await fm.getDataBaseEntry({ name, extension });
        }
        else {
            throw new Error('failed to open stream');
        }
    }


    public getStream(link: string, options: ytdl.downloadOptions = {}): Readable | null {
        let id = this.getId(link);
        if (id) {
            console.log(id);
            let link = 'https://www.youtube.com/watch?v=' + id;
            return ytdl(link, options)
        } else
            return null;
    }

    public getId(url: string): any {
        return Video.extractID(url);
    }

    public queue(url): boolean {

        // this._queue.push();
        return true;
    }

    private commandDownload(message, { args }) {
        if (message.author.id !== '91922008987684864')
            return false;
        if (args.length) {
            let download = this.downloadStream(args[0], { filter: 'audioonly' });
            let reaction;
            download.catch(success => {
                if (reaction) {
                    reaction.remove()
                }
                if (success) {
                    message.react('✅')
                }
                else {
                    message.react('😡')
                }

            }).then(stuff => {
                message.react('🕐').then(_ => {
                    reaction = _;
                    download.then(l => {
                        reaction.remove().then(react => {
                            message.react('✅')
                        })
                    });
                })
            })
        }
    }

}