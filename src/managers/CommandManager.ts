import { BotCommandDataObject, BotCommandList, BotCommandFunc, ManagerList, BotCommandStruct } from "../types/interfaces";
import { Winston } from "winston";
import { Message, Client, RichEmbed } from "discord.js";
import { Manager } from "./Manager";
import Store from "../modules/store/index";
import { isString } from "../helpers/utility";
import { isBoolean } from "util";
import './managers';
import DataManager from "./DataManager";

interface MessageHook {
    (message: Message): any
}

export default class CommandManager extends Manager {

    protected _name: string = 'commandManager';
    private _hooks = {
        message: []
    }

    private hook: RegExp;
    private commands: BotCommandList = {}

    public install() {
        let HOOK = this.store.state.hook;
        this.bot.on('message', (message: Message) => this.onMessageHook(message));

        this.hook = HOOK instanceof RegExp ? HOOK : new RegExp(`^${HOOK} `, 'i');

        let self = this;
        this.installManyCommands({
            hello: {
                handler: this.commandHello.bind(this),
                description: "Say hello to the bot",
            },
            commands: {
                handler: this.commandDisplayCommands.bind(this),
                description: "Get a list of commands"
            }
        })
    }

    public parse(message: Message): BotCommandDataObject | void {
        let raw = message.content;
        let dataManager: DataManager = this.managers.dataManager;


        // Reset hook index
        this.hook.lastIndex = -1;

        if (this.hook.test(raw)) {
            const args: Array<string> = this._getArguments(raw);
            const command: string = this._getCommand(args);

            return {
                command,
                args,
                raw
            }
        }
        return;
    }

    public installCommand(commandName: string, command: BotCommandStruct): void {
        if (!this.commands[commandName]) {
            this.commands[commandName] = command;
        }
        else {
            this.logger.info(`Failed to install command ${commandName}.`)
        }
    }

    public installManyCommands(commandList: BotCommandList) {
        for (let command in commandList) {
            if (commandList.hasOwnProperty(command)) {
                this.installCommand(command, commandList[command]);
            }
        }
    }

    public onMessageHook(message: Message) {
        this._handleCommand(message);
    }

    private _getCommand(args: Array<string>): string {
        let command = args.splice(0, 1)[0];
        return command ? command.toLowerCase() : "";
    }

    private _getArguments(raw: string): Array<string> {
        return raw
            .replace(this.hook, '')
            .trim()
            .split(' ');
    }

    private _handleCommand(message: Message): void {
        let commandData: BotCommandDataObject | void;
        commandData = this.parse(message);
        if (commandData) {
            let { command } = commandData;
            if (this.commands[command]) {
                try {
                    this.commands[command].handler(message, commandData);
                }
                catch (e) {
                    this.logger.error(e);
                    try {
                        if (isString(e)) {
                            message.react(e);
                        }
                        else {
                            if (isBoolean(e)) {
                                message.react(e ? '😒' : '😡')
                            }
                            else {
                                message.react('😡')
                            }
                        }
                    } catch (l) {
                        message.react('😡')
                            .then(reaction => {
                                message.react('💩')
                            })
                    }
                }
            }
        }
    }

    public getCommandNames(): Array<string> {
        return Object.keys(this.commands);
    }

    public extend(): void {

    }

    private commandHello(message: Message) {
        message.channel.send(`What ho, ${message.author.username}!`, { tts: true });
    }

    private commandDisplayCommands(message: Message) {
        let commandNames: Array<string> = this.getCommandNames().sort();
        let embed = new RichEmbed();
        embed.setTitle(`${this.store.state.originalName} Commands`)
            .setDescription(`This bot currently has ${commandNames.length} commands.

To use a command prepend one of the following characters/emojis: 
:fire:, :100:, :ok_hand:, :ok_hand::skin-tone-1:, :ok_hand::skin-tone-2:, :ok_hand::skin-tone-3:, :ok_hand::skin-tone-4:, :ok_hand::skin-tone-5:, $, or £ (God Bless the Queen).

The commands are:`)

        commandNames.forEach(item => {
            embed.addField(item, this.commands[item].description + '.');
        })

        message.channel.send(embed)
        return true;
    }

}