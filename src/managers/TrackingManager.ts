import { Manager } from "./Manager";
import { GuildMember, Message, GuildMemberResolvable, Client, Guild, TextChannel } from "discord.js";
import { ManagerList } from "../types/interfaces";
import './managers';

export default class TrackingManager extends Manager {
    protected _name: string = 'trackingManager';

    private static MARCUS_ID: string = '162651416765267970';

    /**
     * installs manager's functions
     * 
     * @memberof TrackingManager
     */
    public install(): void {
        let self = this;
        this.bot.on('voiceStateUpdate', (night: GuildMember, marcus: GuildMember) => {
            this.nightMarcus(night, marcus);
        });

        this.bot.on('ready', () => {
            this.bot.guilds.forEach((guild: Guild) => {
                this._changeNickname(this.resolver.resolveGuildMember(guild, this.bot.user), this.store.state.originalName);
            })
        })
        this.store.addComputed('channels', {
            get() {
                let channels = self.store.state.channelIDs;
                let keys = Object.keys(channels);

                return keys.reduce((carry, item) => {
                    carry[item] = self.bot.channels.get(channels[item])
                    return carry;
                }, {})

            }
        });

        this.managers.commandManager.installCommand('marcus', {
            handler: this.commandIsMarcus.bind(this),
            description: 'Makes the bot pretend to be Marcus and announce his present status.'
        })
    }

    /**
     * Night Marcus
     * 
     * @param {GuildMember} night 
     * @param {GuildMember} marcus 
     * @memberof TrackingManager
     */
    public nightMarcus(night: GuildMember, marcus: GuildMember): void {
        let guild = night.guild || marcus.guild;
        let guildID = guild.id;
        if (night.voiceChannelID && !marcus.voiceChannelID) {
            if ((night.user.id == TrackingManager.MARCUS_ID) && new Date().getHours() > 20) {
                let botChannelId = this.managers.settingsManager.getGuildSetting(guildID, 'botChannel');

                if (botChannelId) {
                    let botChannel = guild.channels.find('id', botChannelId);
                    if (botChannel) {
                        (botChannel as TextChannel).send('Night Marcus :wave: :crescent_moon: :sleeping: ');
                        (botChannel as TextChannel).send('Night Marcus!', { tts: true }).then((message: Message) => message.delete(100))
                    }
                }
            }
        }
    }

    /**
     * checks if marcus is in a voice channel 
     * 
     * @param {Message} message 
     * @memberof TrackingManager
     */
    public isMarcusHere(message: Message): void {
        let marcus: GuildMember = this._getMarcus(message.guild);
        let self: GuildMember = this._findSelf(message.guild);
        let msg: string = marcus.voiceChannel ? "I'm still here!" : "I've gone for lunch";
        this._changeNickname(self, 'Marcus').then(_ => {
            return message.channel.send(msg, { tts: true });
        }).then(msg => {
            return this._changeNickname(self, this.store.state.originalName)
        })

    }

    /**
     * attempts to change nickname (if bot has permission to)
     * returns a promise with itself as a GuildMember regardless of success
     * 
     * @private
     * @param {GuildMember} self 
     * @param {string} name 
     * @returns {PromiseLike<GuildMember>} 
     * @memberof TrackingManager
     */
    private _changeNickname(self: GuildMember, name: string): PromiseLike<GuildMember> {
        return this._canChangeNickname(self) ?
            self.setNickname(name) :
            new Promise((resolve, reject) => {
                return resolve(self);
            });
    }

    /**
     * returns Marcus as a GuildMember
     * 
     * @private
     * @returns {GuildMember} 
     * @memberof TrackingManager
     */
    private _getMarcus(guild: Guild): GuildMember {
        return guild.member(TrackingManager.MARCUS_ID)
    }

    /**
     * Meditates and finds self as a GuildMember
     * 
     * @private
     * @returns {GuildMember} 
     * @memberof TrackingManager
     */
    private _findSelf(guild: Guild): GuildMember {
        return guild.member(this.bot.user);
    }

    /**
     * returns if the bot has permission to change its name
     * 
     * @private
     * @param {GuildMember} self 
     * @returns {boolean}
     * @memberof TrackingManager
     */
    private _canChangeNickname(self: GuildMember) {
        return self.hasPermission('MANAGE_NICKNAMES') && self.hasPermission("CHANGE_NICKNAME");
    }

    private commandIsMarcus(message: Message) {
        this.isMarcusHere(message);
    }

} 