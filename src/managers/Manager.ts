import { Client, ClientDataResolver } from "discord.js";
import { Winston } from "winston";
import Store from "../modules/store/index";
import { ManagerList } from "../types/interfaces";
import { define } from "../helpers/utility";

/**
 * Interface to make it easier to create generic manager installers
 * 
 * @export
 * @interface ManagerConstructor
 */
export interface ManagerConstructor {
    new(bot: Client, store: Store, logger: Winston): Manager,
    (bot: Client, store: Store, logger: Winston): void,
    readonly prototype;
}

export interface IManager {
    /**
     * Gets the camelCase name for the managers class (stored in _name)
     * allows for possible overriding for more advanced behaviour
     * 
     * @returns {string} 
     * @memberof IManager
     */
    getName(): string;

    /**
     * installs the manager to the bot
     * 
     * @memberof Manager
     */
    install();
}

/**
 * abstract class to provide the fundamentals for a manager to be able to be installed
 * 
 * @export
 * @class Manager
 */
export class Manager implements IManager {
    /**
     * The bot the manager is connected to
     * 
     * @protected
     * @type {Client}
     * @memberof Manager
     */
    protected bot: Client;

    /**
     * The global store attached to the app
     * 
     * @protected
     * @type {Store}
     * @memberof Manager
     */
    protected store: Store;

    /**
     * Logger used for the application
     * 
     * @protected
     * @type {Winston}
     * @memberof Manager
     */
    protected logger: Winston;

    /**
     * Discordjs data resolver
     * 
     * @protected
     * @type {ClientDataResolver}
     * @memberof Manager
     */
    protected resolver: ClientDataResolver

    /**
     * manual way we're setting camelCase for basename
     * 
     * @protected
     * @type {string}
     * @memberof Manager
     */
    protected _name: string = 'manager';

    /**
     * Creates an instance of Manager.
     * @param {Client} bot 
     * @param {Store} store 
     * @param {Winston} logger 
     * @memberof Manager
     */
    constructor(bot: Client, store: Store, logger: Winston) {
        this.bot = bot;
        this.resolver = (bot as any).resolver;
        this.store = store;
        this.logger = logger;
        define(this)('managers', {
            value: store.state.managers
        })
        // this.resolver = new ClientDataResolver(bot);
    }

    /**
     * Gets the camelCase name for the managers class (stored in _name)
     * allows for possible overriding for more advanced behaviour
     * 
     * @returns {string} 
     * @memberof Manager
     */
    public getName(): string {
        return this._name;
    }

    /**
     * installs the manager to the bot
     * 
     * @memberof Manager
     */
    public install(): void {

    }

}