import debounce from 'lodash.debounce';

export {
    getNested,
    coalesce,
    arrayContains,
    arrayPushUnique,
    arrayRemove,
    forceArray,
    define,
    objectRemove,
    isString,
    debounce,
    isUnset,
    isSet,
    promisify
}

export default {
    getNested,
    coalesce,
    arrayContains,
    arrayPushUnique,
    arrayRemove,
    forceArray,
    define,
    objectRemove,
    isString,
    debounce,
    isUnset,
    isSet,
    promisify
}
/**
 * Gets a nested property from an object
 * 
 * @param {*} obj 
 * @param {(any | Array<string | number>)} [path] 
 * @returns {(any | null)} 
 */
function getNested<T>(obj: T): T;
function getNested<T>(obj: T, nothing?: null | undefined): T;
function getNested(obj: any, path: Array<string | number> | string | null): any | null;
function getNested(obj: any, path?: any | Array<string | number>): any | null {
    let argCount: number = arguments.length;
    if (argCount == 1 || (isUnset(path))) {
        return obj;
    }
    if (argCount == 2) {
        if (Array.isArray(arguments[1])) {
            let point = arguments[0];
            let array = arguments[1];
            for (let i = 0; i < array.length && point != null; i++) {
                point = coalesce(point[array[i]], null);
            }
            return point;
        }
        else if (isString(arguments[1])) {
            let normalizedArguments = arguments[1].replace(/\[(.*)\]/g, '.$1');
            return getNested(arguments[0], normalizedArguments.split`.`);
        }
        throw new Error("Unknown second argument provided.")
    }
    if (argCount > 2) {
        let argSlice = Array.prototype.slice.call(arguments, 1);
        return getNested(arguments[0], argSlice);
    }
    return null;
}

/**
 * Gets a nested item from an object, returns resultIfNull if it cannot
 * get the nested property (due to one of the nested items being null)
 * 
 * @param {*} obj 
 * @param {(string | Array<string | number> | null)} [nested] 
 * @param {(any | null)} [resultIfNull] 
 * @returns {(any | null)} 
 */
function coalesce<T>(obj: T, nested?: null | undefined): T;
function coalesce<T>(obj: null, valueIfNull: T): T;
function coalesce<T>(obj: any, nested: string | Array<string | number>, valueIfNull: T): any | T;
function coalesce(obj: any, nested?: string | Array<string | number> | null, resultIfNull?: any | null): any | null {
    let argCount = arguments.length;
    let value;
    if (argCount === 2) {
        value = arguments[0];
        return value ? value : arguments[1];
    }
    if (argCount === 3) {
        value = getNested(arguments[0], arguments[1]);
        return coalesce(value, arguments[2])
    }
    if (argCount > 3) {
        let argSlice = Array.prototype.slice.apply(arguments, [1, argCount - 1]);
        value = getNested(arguments[0], argSlice);
        // console.log(argSlice,value);
        return coalesce(value, arguments[argCount - 1]);
    }
    return null;
}
/**
 * Pushes a given value to the array but only if it doesn't already exist in the array
 * 
 * @param {Array<any>} arr 
 * @param {*} item 
 * @returns {number} 
 */
function arrayPushUnique(arr: Array<any>, item: any): number {
    let index = arr.indexOf(item);
    if (index === -1) {
        return arr.push(item);
    }
    return -1;
}
/**
 * Removes an item from the array if it exists
 * 
 * @param {Array<any>} arr 
 * @param {*} item 
 * @returns {(any | null)} 
 */
function arrayRemove(arr: Array<any>, item: any): any | null {
    let index = arr.indexOf(item);
    if (index !== -1) {
        return arr.splice(index, 1);
    }
    return null;
}
/**
 * returns whether or not a value exists within the given array
 * 
 * @param {Array<any>} arr 
 * @param {*} item 
 * @returns {boolean} 
 */
function arrayContains(arr: Array<any>, item: any): boolean {
    return arr.indexOf(item) > -1;
}

/**
 * forces a single array output from one or many inputs
 * 
 * @param {(Array<any> | any | undefined | null)} obj 
 * @param {...Array<any>} otherObj 
 * @returns {Array<any>} 
 */
function forceArray<T extends ArrayLike<any>>(obj: T): T;
function forceArray(...manyArguments: Array<any>): Array<any>;
function forceArray(obj: Array<any> | any | undefined | null, ...otherObj: Array<any>): Array<any> {
    let numArgs: number = otherObj.length;
    if (numArgs == 0) {
        if (obj != null) {
            return Array.prototype.slice.apply(arguments);
        }
        else {
            return []
        }
    }
    return Array.isArray(arguments[0]) ? arguments[0] : (arguments[0] === undefined) ? [] : [arguments[0]];
}
/**
 * Returns a function that allows for easily adding properties to first argument
 * 
 * @param {object} object 
 * @returns {(property: string, value: PropertyDescriptor) => void} 
 */
function define(object: object): (property: string, value: PropertyDescriptor) => void {
    return function (property: string, value: PropertyDescriptor): void {
        return Object.defineProperty(object, property, value);
    }
}
/**
 * Removes a property from an Object
 * 
 * @param {object} obj 
 * @param {string} key 
 */
function objectRemove(obj: object, key: string): void {
    if (obj[key] !== undefined) {
        delete obj[key];
    }
}
/**
 * Returns if given object is a string
 * 
 * @param {*} obj 
 * @returns {obj is string} 
 */
function isString(obj: any): obj is string {
    return typeof obj === 'string';
}

/**
 * returns whether the given value is null or undefined
 * 
 * @param {*} item 
 * @returns {boolean} 
 */
function isUnset(item: any): item is null | undefined {
    return item === null || item === undefined;
}

/**
 * returns if an item is set
 * 
 * @template T 
 * @param {T} item 
 * @returns {item is T} 
 */
function isSet<T>(item: T): item is T {
    return !isUnset(item);
}

function promisify(fn: Function, thisArg: any = null): (...args: Array<any>) => Promise<any> {
    let t = this;
    return function () {
        let arr = Array.prototype.slice.call(arguments);
        return new Promise((resolve, reject) => {
            let outfn = function () {
                let m: Array<any> = Array.prototype.slice.call(arguments);
                let err = m.shift();
                if (err)
                    return reject(err)
                return resolve(...m);
            }
            arr.push(outfn);
            fn.apply(thisArg, arr);
        });
    };
}