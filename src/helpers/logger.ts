import * as logger from "winston";

logger.configure({
    level: 'debug',
    transports: [
        new logger.transports.Console({
            colorize: true
        })
    ]
});


logger.remove(logger.transports.Console);
logger.add(logger.transports.Console, {
    colorize: true,
    level:'debug'
});

export default logger;