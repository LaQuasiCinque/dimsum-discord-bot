import env from './modules/env';

import { bot } from './Bot'
import db from './database'
import Store from './modules/store'
import { Message, User, RichEmbed, Guild, GuildMember } from 'discord.js';

import state from './modules/store/state';
import mutators from './modules/store/mutators';

import AudioManager from './managers/AudioManager'
import CommandManager from './managers/CommandManager'
import YoutubeManager from './managers/YoutubeManager';
import TrackingManager from './managers/TrackingManager';
import IntroManager from './managers/IntroManager';
import DataManager from './managers/DataManager';
import PathOfExileManager from './managers/PathOfExile/PathOfExileManager';
import logger from './helpers/logger';
import { SettingsManager } from './managers/SettingsManager';

import { FileManager } from './managers/FileManager';

db.on('open', function () {
    logger.info("Connected to database")
})



function s(buffer) {
    let framePosition = String.prototype.indexOf.call(buffer, (new Buffer("ID3")));
    if (framePosition == -1 || framePosition > 20) {
        return -1;
    } else {
        return framePosition;
    }
}



const store: Store = new Store(state, mutators);

store.commit('setBot', bot);

const dataManager: DataManager = store.commit('addManager', DataManager);
const commandManager: CommandManager = store.commit('addManager', CommandManager);
const settingsManager: SettingsManager = store.commit('addManager', SettingsManager);
const trackingManager: TrackingManager = store.commit('addManager', TrackingManager);
const audioManager: AudioManager = store.commit('addManager', AudioManager);
const youtubeManager: YoutubeManager = store.commit('addManager', YoutubeManager);
const pathOfExileManager: PathOfExileManager = store.commit('addManager', PathOfExileManager);
const introManager: IntroManager = store.commit('addManager', IntroManager);
const fileManager: FileManager = store.commit('addManager', FileManager);

commandManager.installCommand('purge', {
    handler(message: Message, { args }) {
        message.channel.fetchMessages({
            limit: 100
        }).then(data => {
            let msgs = data.filter(item => {
                return item.author.id === bot.user.id
            })
            Promise.all(msgs.map(item => {
                return item.delete();
            })).then(items => {
                message.react('😫').then(item => {
                    message.react('👌')
                })
            })
        }).catch(data => {
            console.error(data)
        })
    },
    description: 'Makes the bot try to delete its last few messages from the current channel.'
})



logger.info('Logging in with token ' + env.TOKEN);
bot.login(env.TOKEN)



const http = require('http');
const express = require('express');
const app = express();

const port = process.env.port || 3000;

app.listen(port, () => {
    console.log('Running');
});
