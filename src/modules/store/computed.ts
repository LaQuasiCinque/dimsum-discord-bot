import {define} from '../../helpers/utility'

const computed = {}
const addComputed = define(computed);


export {
    computed,
    addComputed,
    removeComputed
}

function removeComputed(name: string){
    if(computed[name])
        delete computed[name];
}