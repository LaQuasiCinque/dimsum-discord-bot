import { computed, addComputed, removeComputed } from './computed'
import { StateInterface } from './state'
import { define } from '../../helpers/utility';

export interface PropertyDescriptorList {
    [propName: string]: PropertyDescriptor
}

export interface Mutator {
    (store: Store, payload?: any): any | null | undefined
};

export interface MutatorList {
    [propName: string]: Mutator
}

export default class Store {
    public state: StateInterface;

    public getters: any;
    private _mutators: MutatorList
    private _selfDefine: (key: string, props: PropertyDescriptor) => any;

    constructor(state: StateInterface, mutators: MutatorList) {
        this.state = state;
        this._mutators = mutators;
        let _selfDefine = define(this);
        _selfDefine('_selfDefine', {
            value: _selfDefine,
            writable: false,
            enumerable: false
        });

        _selfDefine('getters', {
            get() {
                return computed
            },
        })


    }

    public addComputed(keyValuePairs: PropertyDescriptorList): void
    public addComputed(key: string, value: PropertyDescriptor): void
    public addComputed(keyOrPairs: string | PropertyDescriptorList, value?: PropertyDescriptor): void {
        if (typeof keyOrPairs === 'object') {
            for (let key in (keyOrPairs as PropertyDescriptorList)) {
                if (keyOrPairs.hasOwnProperty(key)) {
                    addComputed(key, keyOrPairs[key]);
                }
            }
        }
        else
            if (value)
                addComputed(keyOrPairs, value);
    }

    public removeComputed(key: string): void {
        removeComputed(key);
    }

    public commit(name: string, payload?: any): any | null | undefined {
        if (!this._mutators[name])
            return;
        return this._mutators[name](this, payload);
    }
}

