import Store from "./index";
import { Manager, ManagerConstructor } from "../../managers/Manager";
import logger from "../../helpers/logger";
import { Client } from "discord.js";

export default {
    addManager,
    setBot,
}

function addManager(store: Store, manager: ManagerConstructor): Manager {
    let newManager: Manager = new manager((store.state.bot as Client), store, logger);
    store.state.managers[newManager.getName()] = newManager;
    newManager.install();
    return newManager;
}

function setBot(store: Store, bot: Client) {
    store.state.bot = bot;
}