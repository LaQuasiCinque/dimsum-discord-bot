import CommandManager from "../../managers/CommandManager";
import { ManagerList } from "../../types/interfaces";
import env from '../env';

export interface StateInterface {
    managers: ManagerList,
    [propName: string]: any
}

const state: StateInterface = {
    hook: /^(?:🔥|💯|👌(?:🏻|🏼|🏽|🏾|🏿)?|£|\$)/,
    managers: {},
    originalName: env.BOT_NAME || 'GG-Int-bot'
}

export default state;