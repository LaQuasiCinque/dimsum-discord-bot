import * as mongoose from 'mongoose'
import logger from '../../helpers/logger';

const Schema = new mongoose.Schema({
    guildId: { type: String, required: true }
}, { strict: false })

Schema.statics.findOneOrCreate = async function findOneOrCreate(condition, doc) {
    let one;
    try {
        one = await this.findOne(condition);
    }
    catch (e) {
        logger.error(e);
    }
    return one || this.create(doc);
}


let Model: mongoose.Model<mongoose.Document> & {
    findOneOrCreate: (condition: any, doc: any) => Promise<any>
};

Model = mongoose.model('GuildSetting', Schema, 'guildSettings') as mongoose.Model<mongoose.Document> & {
    findOneOrCreate: (condition: any, doc: any) => Promise<any>
};

export default Model;

export {
    Model,
    Schema
}