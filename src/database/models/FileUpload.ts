import * as mongoose from 'mongoose'

import * as Youtube from 'simple-youtube-api';
import * as Video from 'simple-youtube-api/src/structures/Video';
import logger from '../../helpers/logger';

const Schema = new mongoose.Schema({
    name: { type: String, required: true, unique: true },
    path: { type: String, default: '' },
    extension: { type: String, default: '' },
    createdDate: { type: Date, required: true },
    updatedDate: { type: Date, default: Date.now },
})

Schema.pre('validate', function (next) {
    if (!((this as any).createdDate)) {
        (this as any).createdDate = Date.now()
    }
    next()
})

Schema.statics.findOneOrCreate = async function findOneOrCreate(condition, doc) {
    let one;
    try {
        one = await this.findOne(condition);
    }
    catch (e) {
        logger.error(e);
    }
    return one || this.create(doc);
}


let FileUpload: mongoose.Model<mongoose.Document> & {
    findOneOrCreate: (condition: any, doc: any) => Promise<any>
};

FileUpload = mongoose.model('FileUpload', Schema, 'fileuploads') as mongoose.Model<mongoose.Document> & {
    findOneOrCreate: (condition: any, doc: any) => Promise<any>,
};;

export type FileUploadType = {
    name: string,
    path: string,
    extension: string,
    createdDate: Date,
    updatedDate: Date,
}


export default FileUpload;

export {
    FileUpload,
    Schema,
}
