import * as mongoose from 'mongoose'
import { Schema as IntroSchema } from './Intros'
import logger from '../../helpers/logger';

const Schema = new mongoose.Schema({
    userId: { type: String, required: true, unique: true },
    name: { type: String, default: null },
})

Schema.statics.findOneOrCreate = async function findOneOrCreate(condition, doc) {
    let one;
    try {
        one = await this.findOne(condition);
    }
    catch (e) {
        logger.error(e);
    }
    return one || this.create(doc);
}

Schema.pre('validate', function (next) {
    if (!((this as any).createdDate)) {
        console.log("adding createdDate");
        (this as any).createdDate = Date.now()
    }
    next()
})


let Model: mongoose.Model<mongoose.Document> & {
    findOneOrCreate: (condition: any, doc: any) => Promise<any>
};

Model = mongoose.model('User', Schema, 'users') as mongoose.Model<mongoose.Document> & {
    findOneOrCreate: (condition: any, doc: any) => Promise<any>
};;




export default Model;
