import { Schema } from "mongoose";

export default function timestampsPlugin(schema: Schema, options: any) {
    schema.add({
        createdAt: {
            type: Date,
        },
        updatedAt: {
            type: Date,
            default: Date.now
        }
    })

    schema.pre("save", function (next) {
        let now = new Date()
        if (!(this as any).createdAt) {
            (this as any).createdAt = now
        }
        (this as any).updatedAt = now;
        next()
    })

}
