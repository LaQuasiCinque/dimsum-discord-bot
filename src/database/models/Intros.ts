import * as mongoose from 'mongoose'

import * as Youtube from 'simple-youtube-api';
import * as Video from 'simple-youtube-api/src/structures/Video';

const Schema = new mongoose.Schema({
    url: {
        type: String,
        required: true,
        validate(v) {
            let id = Video.extractID(v)
            return id === undefined;
        },
        get(v) {
            return `https://www.youtube.com/watch?v=${v}`
        },
        set(v) {
            return Video.extractID(v);
        }
    },
    volume: { type: Number, default: 0.2, max: 2 },
    start: { type: Number, default: null },
    length: { type: Number, default: null },
    createdDate: { type: Date, required: true },
    updatedDate: { type: Date, default: Date.now },
    userId: { type: String, required: true },
    fileUploadId: { type: String }
})

Schema.pre('validate', function (next) {
    if (!((this as any).createdDate)) {
        (this as any).createdDate = Date.now()
    }
    next()
})



let Model: mongoose.Model<mongoose.Document> & {
    findOneOrCreate: (condition: any, doc: any) => Promise<any>
};

Model = mongoose.model('Intro', Schema, 'intros') as mongoose.Model<mongoose.Document> & {
    findOneOrCreate: (condition: any, doc: any) => Promise<any>
};;


export default Model;

export {
    Model,
    Schema
}
