import * as mongoose from 'mongoose';
import { SchemaType } from 'mongoose'
import env from '../modules/env'

mongoose.connect(`mongodb://${env.DB_USER}:${env.DB_PASS}@${env.DB_URI}`)

const connection: mongoose.Connection = mongoose.connection;
export default connection;