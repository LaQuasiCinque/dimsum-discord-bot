import * as Discord from 'discord.js'
import { Manager, IManager } from '../managers/Manager';

export interface BotCommandFunc {
    (
        message: Discord.Message,
        data: BotCommandDataObject
    ): boolean | string | void;
}

export interface BotCommandDataObject {
    command: string,
    raw: string,
    args: Array<string>,
    // getData: (
    //     path: string,
    //     key?: string
    // ) => any,
    // setData: (
    //     path: string,
    //     key: string | null,
    //     value: object | string
    // ) => boolean,
    [propName: string]: any,
}

export interface BotCommandList {
    [propName: string]: BotCommandStruct
}

export interface BotCommandStruct {
    handler: BotCommandFunc,
    description: string,
    arguments?: Array<object>
}


export interface ManagerList {
    [propName: string]: IManager
}